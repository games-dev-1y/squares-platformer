﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

/* Class Description:
 *  -   To keep in line with the Game class containing only 1 copy of anything repeated.
 */

namespace JointProject2
{
    class SoundCollection
    {
        //
        ///
        //// VARIABLES
        ///
        //

        private SoundEffect
            moButtonHover,
            moButtonClick;
        private Song 
            moMainMenuSong;
        private Song[]
            maLevelSongs;

        //
        ///
        //// CONSTANT DECLARATIONS
        ///
        //

        private const string SongLevelExtension =
            " Soundtrack";

        //
        ///
        //// PUBLIC PROPERTIES
        ///
        //

        /// <summary>
        /// 
        /// </summary>
        public Song MainMenuSong
        { get { return moMainMenuSong; } }

        //
        ///
        //// CONSTRUCTOR
        ///
        //

        /// <summary>
        /// 
        /// </summary>
        public SoundCollection()
        {
            maLevelSongs = new Song[Game.MaxLevel];
        }

        //
        ///
        //// PUBLIC METHODS
        ///
        //

        /// <summary>
        /// Will load all external sound content into their appropriate containers.
        /// </summary>
        /// <param name="loAssetsManager"></param>
        public void LoadContent(ContentManager loAssetsManager)
        {
            moMainMenuSong = loAssetsManager.Load<Song>(FilePaths.SoundMainMenuIntro);
            for (int ldIndex = 0; ldIndex < maLevelSongs.Length; ldIndex++)
            {
                maLevelSongs[ldIndex] = loAssetsManager.Load<Song>(FilePaths.SoundLevels + (ldIndex + 1).ToString() + SongLevelExtension);
            }
        }
        /// <summary>
        /// Overloaded Method:
        /// Will Play the Passed Song
        /// </summary>
        /// <param name="loSong">
        /// Passed Song will determine what is played.
        /// </param>
        public void Play(Song loSong)
        {
            MediaPlayer.Play(loSong);
        }
        /// <summary>
        /// Overloaded Method:
        /// Will play the Passed SoundEffect
        /// </summary>
        /// <param name="loSoundEffect">
        /// Passed SoundEffect will determine what is played.
        /// </param>
        public void Play(SoundEffect loSoundEffect)
        {
            loSoundEffect.Play();
        }
        /// <summary>
        /// Will play the song based on the Passed int level.
        /// (Between 1 to 5)
        /// </summary>
        /// <param name="ldLevel">
        /// Passed int that will define what song of what level is played.
        /// </param>
        public void PlayLevelSong(int ldLevel)
        {
            switch (ldLevel)
            {
                case 5:
                case 4:
                case 3:
                case 2:
                case 1:
                    Play(maLevelSongs[ldLevel - 1]);
                    break;
                default:
                    break;
            }
        }

    }
} // End of namespace