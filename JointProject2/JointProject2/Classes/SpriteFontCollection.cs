﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

/* Class Description:
 *  Using the Design Pattern known as, Flyweight Pattern.
 *  This class will hold all the spritefonts as public to be accessible for the DrawString statements.
 */

namespace JointProject2
{
    class SpriteFontCollection
    {
        //
        /* 
         * VARIABLES
         */
        //

        private SpriteFont
            moTitleTextFont,
            moMainMenuTextFont,
            moOptionsMenuTextFont,
            moGameTextFont;

        //
        /* 
         * PROPERTIES
         */
        //

        /// <summary>
        /// Gets the Title's Text Font SpriteFont.
        /// </summary>
        public SpriteFont TitleFont
        { get { return moTitleTextFont; } }
        /// <summary>
        /// Gets the Main Menu Text Font SpriteFont.
        /// </summary>
        public SpriteFont MainMenuTextFont
        { get { return moMainMenuTextFont; } }
        /// <summary>
        /// Gets the Options Menu Text Font SpriteFont.
        /// </summary>
        public SpriteFont OptionsMenuText
        { get { return moOptionsMenuTextFont; } }
        /// <summary>
        /// Gets the Game's Text Font SpriteFont.
        /// </summary>
        public SpriteFont GameTextFont
        { get { return moGameTextFont; } }

        //
        /* 
         * CONSTRUCTORS
         */
        //

        /// <summary>
        /// Default Constructor,
        /// will initialize our instance variables.
        /// </summary>
        public SpriteFontCollection()
        { }

        //
        /*
         * PUBLIC METHODS
         */
        //

        /// <summary>
        /// Will load in all the external texture files
        /// </summary>
        /// <param name="loAssetsManager">
        /// Passed Assets Manager for our content pipeline
        /// </param>
        public void LoadContent(ContentManager loAssetsManager)
        {
            moTitleTextFont = loAssetsManager.Load<SpriteFont>(FilePaths.MenuTitleFont);
            moMainMenuTextFont = loAssetsManager.Load<SpriteFont>(FilePaths.MainMenuFont);
            moOptionsMenuTextFont = moMainMenuTextFont;
            moGameTextFont = loAssetsManager.Load<SpriteFont>(FilePaths.SquareFont);
        }

    }
} // End of namespace