﻿// No using's required in this class.

/* Class Description:
 *  Will contain all file paths for our game,
 *  declared as public static so that we need not declare our File Paths class,
 *  in multiple locations
 */

namespace JointProject2
{
    public class FilePaths
    {
        //
        /*
         * CONSTANT DECLARATION
         */
        //

        /* String File / Paths Declarations */

        // Content Folders
        private const string AssetsFolder =
            "Assets/";
        private const string ArtFolder =
            AssetsFolder + "Art/";
        private const string SoundFolder =
            AssetsFolder + "Sound/";

        #region Art Assets
        /* BUTTONS */
        private const string ButtonFolder =
            ArtFolder + "Button/";
        private const string ButtonActiveTexture =
            ButtonFolder + "button_active";
        private const string ButtonDeactiveTexture =
            ButtonFolder + "button_deactive";
        private const string ButtonHoveredTexture =
            ButtonFolder + "button_hovered";
        private const string ButtonPressedTexture =
            ButtonFolder + "button_pressed";
        /* SQUARE WORLD */
        private const string SquareFolder =
            ArtFolder + "Square/";
        private const string ParticleTextureFile =
            SquareFolder + "Particle";
        private const string EmptySquareTextureFile =
            SquareFolder + "EmptySquare";
        private const string WallSquareTextureFile =
            SquareFolder + "WallSquare";
        private const string ExitSquareTextureFile =
            SquareFolder + "ExitSquare";
        private const string PlayerSquareTextureFile =
            SquareFolder + "PlayerSquare";
        private const string PlayerSquareEyeTextureFile =
            SquareFolder + "PlayerEye";
        private const string SquareFontFile =
            SquareFolder + "GameFont";
        /* SQUARE ENEMIES */
        private const string EnemiesFolder =
            SquareFolder + "Enemies/";
        private const string EnemyBasicSquareTextureFile =
            EnemiesFolder + "EnemyBasicSquare";
        private const string EnemyTurretSquareTextureFile =
            EnemiesFolder + "EnemyTurretSquare";
        private const string EnemyTurretEyeTextureFile =
            EnemiesFolder + "EnemyTurretEye";
        private const string EnemyTurretBulletTextureFile =
            EnemiesFolder + "EnemyTurretBullet";
        /* GAME STATES FOLDER */
        private const string GameStatesFolder =
            ArtFolder + "Game States/";
        /* MENU TITLE */
        private const string MenuTitleSpriteFont =
            GameStatesFolder + "MenuTitleFont";
        private const string MenuTitlePlaceholderTexture =
            GameStatesFolder + "TextPlaceholder";
        /* SPLASH SCREEN */
        private const string SplashScreenFolder =
            GameStatesFolder + "Splash Screen/";
        private const string SplashScreenBackgroundTextureFile =
            SplashScreenFolder + "Background Texture";
        private const string SplashScreenForegroundTextureFile =
            SplashScreenFolder + "Foreground Texture";
        private const string SplashScreenSelectionBarTextureFile =
            SplashScreenFolder + "Selection Bar";
        private const string SplashScreenSelectionSelectionTextureFile =
            SplashScreenFolder + "SelectionPlaceholder";
        private const string SplashScreenSpriteFont =
            SplashScreenFolder + "SplashScreenFont";
        private const string SplashScreenTitleSpriteFont =
            SplashScreenFolder + "SplashScreenTitleFont";
        private const string SplashScreenPlaceholderTexture =
            SplashScreenFolder + "TextPlaceholder";
        /* MAIN MENU */
        private const string MainMenuFolder =
            GameStatesFolder + "Main Menu/";
        private const string MainMenuSpriteFont =
            MainMenuFolder + "MainMenuFont";
        private const string MainMenuBackgroundTexture =
            MainMenuFolder + "Background Texture";
        private const string MainMenuPlaceholderTexture =
            MainMenuFolder + "Text Placeholder";
        #endregion

        #region Sound Assets
        /* BUTTON FOLDER */
        private const string SoundButtonFolder =
            SoundFolder + "Button/";
        /* GAME STATES FOLDER */
        private const string SoundGameStatesFolder =
            SoundFolder + "GameStates/";
        // Main Menu Folder
        private const string SoundMainMenuFolder =
            SoundGameStatesFolder + "Main Menu/";
        private const string SoundMainMenuSoundTrack =
            SoundMainMenuFolder + "MainMenu Soundtrack";
        // Splash Screen Folder
        private const string SoundSplashScreenFolder =
            SoundGameStatesFolder + "Splash Screen/";
        private const string SoundSplashScreenSoundTrack =
            SoundSplashScreenFolder + "Intro Soundtrack";
        /* LEVELS FOLDER */
        private const string SoundLevelsFolder =
            SoundFolder + "Levels/";
        private const string SoundLevelSoundTrack =
            SoundLevelsFolder + "Level ";
        #endregion

        // Level Assets
        private const string LevelsFolder =
            @"Levels\";
        private const string LevelFile =
            LevelsFolder + "Level";
        private const string Level1File =
            LevelsFolder + "Level1.csv";
        private const string Level2File =
            LevelsFolder + "Level2.csv";
        private const string Level3File =
            LevelsFolder + "Level3.csv";
        private const string Level4File =
            LevelsFolder + "Level4.csv";
        private const string Level5File =
            LevelsFolder + "Level5.csv";
        /* SAVE GAMES */
        private const string SaveGameFolderPath =
            @"SaveGames\";

        //
        /*
         * PUBLIC METHODS
         */
        //

        #region Public Static Methods for Art Assets
        /// <summary>
        /// Gets the Empty Square Texture File Path and Name
        /// </summary>
        public static string SquareEmptyTexture
        { get { return EmptySquareTextureFile; } }
        /// <summary>
        /// Gets the Wall Square Texture File Path and Name
        /// </summary>
        public static string SquareWallTexture
        { get { return WallSquareTextureFile; } }
        /// <summary>
        /// Gets the Exit Square Texture File Path and Name.
        /// </summary>
        public static string SquareExitTexture
        { get { return ExitSquareTextureFile; } }
        /// <summary>
        /// Gets the Player Square Texture File Path and Name
        /// </summary>
        public static string SquarePlayerTexture
        { get { return PlayerSquareTextureFile; } }
        /// <summary>
        /// Gets the Player Eye Square Texture File Path and Name
        /// </summary>
        public static string SquarePlayerEyeTexture
        { get { return PlayerSquareEyeTextureFile; } }
        /// <summary>
        /// Gets the Particle Square Texture File Path and Name.
        /// </summary>
        public static string SquareParticleTexture
        { get { return ParticleTextureFile; } }
        /// <summary>
        /// Gets the Enemy Basic Square Texture File Path and Name
        /// </summary>
        public static string SquareEnemyBasicTexture
        { get { return EnemyBasicSquareTextureFile; } }
        /// <summary>
        /// Gets the Enemy Turret Square Texture File Path and Name
        /// </summary>
        public static string SquareEnemyTurretTexture
        { get { return EnemyTurretSquareTextureFile; } }
        /// <summary>
        /// Gets the Enemy Turret Eye Square Texture File Path and Name
        /// </summary>
        public static string SquareEnemyTurretEyeTexture
        { get { return EnemyTurretEyeTextureFile; } }
        /// <summary>
        /// Gets the Enemy Turret Bullet Square Texture File Path and name
        /// </summary>
        public static string SquareEnemyTurretBulletTexture
        { get { return EnemyTurretBulletTextureFile; } }
        /// <summary>
        /// Gets the Square Font File Path and Name
        /// </summary>
        public static string SquareFont
        { get { return SquareFontFile; } }
        /// <summary>
        /// Gets the Button Active Texture's File Path and Name
        /// </summary>
        public static string ButtonActive
        { get { return ButtonActiveTexture; } }
        /// <summary>
        /// Gets the Button Deactive Texture's File Path and Name
        /// </summary>
        public static string ButtonDeactive
        { get { return ButtonDeactiveTexture; } }
        /// <summary>
        /// Gets the Button Hover Texture's File Path and Name
        /// </summary>
        public static string ButtonHover
        { get { return ButtonHoveredTexture; } }
        /// <summary>
        /// Gets the Button Press Texture's File Path and Name
        /// </summary>
        public static string ButtonPress
        { get { return ButtonPressedTexture; } }
        /// <summary>
        /// Gets the Menu Title SpriteFont File Path and Name.
        /// </summary>
        public static string MenuTitleFont
        { get { return MenuTitleSpriteFont; } }
        /// <summary>
        /// Gets the Menu Title Placeholder File Path and Name.
        /// </summary>
        public static string MenuTitlePlaceholder
        { get { return MenuTitlePlaceholderTexture; } }
        /// <summary>
        /// Gets the Splash Screen Background Texture File Path and Name
        /// </summary>
        public static string SplashScreenBackgroundTexture
        { get { return SplashScreenBackgroundTextureFile; } }
        /// <summary>
        /// Gets the Splash Screen Foreground Texture File Path and Name
        /// </summary>
        public static string SplashScreenForegroundTexture
        { get { return SplashScreenForegroundTextureFile; } }
        /// <summary>
        /// Gets the Splash Screen Placeholder Texture File Path and Name
        /// </summary>
        public static string SplashScreenPlaceholder
        { get { return SplashScreenPlaceholderTexture; } }
        /// <summary>
        /// Gets the Splash Screen Selection Bar Texture File Path and Name
        /// </summary>
        public static string SplashScreenSelectionBarTexture
        { get { return SplashScreenSelectionBarTextureFile; } }
        /// <summary>
        /// Gets the Splash Screen Selection Placeholder Texture File Path and Name
        /// </summary>
        public static string SplashScreenSelectionPlaceholderTexture
        { get { return SplashScreenSelectionSelectionTextureFile; } }
        /// <summary>
        /// Gets the Splash Screen SpriteFont File Path and Name
        /// </summary>
        public static string SplashScreenFont
        { get { return SplashScreenSpriteFont; } }
        /// <summary>
        /// Gets the Splash Screen SpriteFont Title File Path and Name
        /// </summary>
        public static string SplashScreenTitleFont
        { get { return SplashScreenTitleSpriteFont; } }
        /// <summary>
        /// Gets the Main Menu SpriteFont File Path and Name
        /// </summary>
        public static string MainMenuFont
        { get { return MainMenuSpriteFont; } }
        /// <summary>
        /// Gets the Main Menu Texture File Path and Name
        /// </summary>
        public static string MainMenuTexture
        { get { return MainMenuBackgroundTexture; } }
        /// <summary>
        /// Gets the Main Menu Placeholer Texture File Path and Name
        /// </summary>
        public static string MainMenuPlaceholder
        { get { return MainMenuPlaceholderTexture; } }
        /// <summary>
        /// Gets the Level File Path as a string (examplefolder/file)
        /// </summary>
        public static string Level
        { get { return LevelFile; } }
        /// <summary>
        /// Gets the Level 1 File Path as a string (examplefolder/file.extension)
        /// </summary>
        public static string Level1
        { get { return Level1File; } }
        /// <summary>
        /// Gets the Level 2 File Path as a string (examplefolder/file.extension)
        /// </summary>
        public static string Level2
        { get { return Level2File; } }
        /// <summary>
        /// Gets the Level 3 File Path as a string (examplefolder/file.extension)
        /// </summary>
        public static string Level3
        { get { return Level3File; } }
        /// <summary>
        /// Gets the Level 4 File Path as a string (examplefolder/file.extension)
        /// </summary>
        public static string Level4
        { get { return Level4File; } }
        /// <summary>
        /// Gets the Level 5 File Path as a string (examplefolder/file.extension)
        /// </summary>
        public static string Level5
        { get { return Level5File; } }
        #endregion

        #region Public Static Methods for Sound Assets
        /// <summary>
        /// Gets the Main Menu Intro Soundtrack File Path and Name.
        /// </summary>
        public static string SoundMainMenuIntro
        { get { return SoundMainMenuSoundTrack; } }
        /// <summary>
        /// Gets the Splash Screen Soundtrack File Path and Name.
        /// </summary>
        public static string SoundSplashScreenSoundtrack
        { get { return SoundSplashScreenSoundTrack; } }
        /// <summary>
        /// Gets the Levels Soundtrack File Path and Name with missing extension.
        /// </summary>
        public static string SoundLevels
        { get { return SoundLevelSoundTrack; } }
        #endregion

        /// <summary>
        /// Gets the Save Game Folder Path
        /// </summary>
        public static string SaveGameFolder
        { get { return SaveGameFolderPath; } }

    }
} // End of namespace