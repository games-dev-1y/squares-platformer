﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

/* Class Description:
 *  Using the Design Pattern known as, Flyweight Pattern.
 *  This class will hold all the textures as public to be accessible for the Draw statement
 */

namespace JointProject2
{
    class TextureCollection
    {
        //
        /* 
         * VARIABLES
         */
        //

        private Texture2D
            moEmptySquare,
            moWallSquare,
            moExitSquare,
            moPlayerSquare,
            moPlayerEyeSquare,
            moEnemyBasicSquare,
            moEnemyTurretSquare,
            moEnemyTurretEye,
            moEnemyTurretBullet,
            moButtonActiveTexture,
            moButtonDeactiveTexture,
            moButtonHoverTexture,
            moButtonPressTexture,
            moMainMenuTexture,
            moPlaceholderTexture,
            moParticleTexture;

        //
        /* 
         * PROPERTIES
         */
        //

        /// <summary>
        /// Gets the Empty Square Texture
        /// </summary>
        public Texture2D EmptySquare
        { get { return moEmptySquare; } }
        /// <summary>
        /// Gets the Wall Square Texture
        /// </summary>
        public Texture2D WallSquare
        { get { return moWallSquare; } }
        /// <summary>
        /// Gets the Exit Square Texture
        /// </summary>
        public Texture2D ExitSquare
        { get { return moExitSquare; } }
        /// <summary>
        /// Gets the Player Square Texture
        /// </summary>
        public Texture2D PlayerSquare
        { get { return moPlayerSquare; } }
        /// <summary>
        /// Gets the Player Eye Square Texture
        /// </summary>
        public Texture2D PlayerEyeSquare
        { get { return moPlayerEyeSquare; } }
        /// <summary>
        /// Gets the Enemy Basic Square Texture
        /// </summary>
        public Texture2D EnemyBasicSquare
        { get { return moEnemyBasicSquare; } }
        /// <summary>
        /// Gets the Enemy Turret Square Texture
        /// </summary>
        public Texture2D EnemyTurretSquare
        { get { return moEnemyTurretSquare; } }
        /// <summary>
        /// Gets the Enemy Turret Eye Square Texture
        /// </summary>
        public Texture2D EnemyTurretEyeSquare
        { get { return moEnemyTurretEye; } }
        /// <summary>
        /// Gets the Enemy Turret Bullet Square Texture
        /// </summary>
        public Texture2D EnemyTurretBullet
        { get { return moEnemyTurretBullet; } }
        /// <summary>
        /// Gets the Button's Active Texture
        /// </summary>
        public Texture2D ButtonActive
        { get { return moButtonActiveTexture; } }
        /// <summary>
        /// Gets the Button's Deactive Texture
        /// </summary>
        public Texture2D ButtonDeactive
        { get { return moButtonDeactiveTexture; } }
        /// <summary>
        /// Gets the Button's Hover Texture
        /// </summary>
        public Texture2D ButtonHover
        { get { return moButtonHoverTexture; } }
        /// <summary>
        /// Gets the Button's Press Texture
        /// </summary>
        public Texture2D ButtonPress
        { get { return moButtonPressTexture; } }
        /// <summary>
        /// Gets the Main Menu Background Texture.
        /// </summary>
        public Texture2D MainMenuBackground
        { get { return moMainMenuTexture; } }
        /// <summary>
        /// Gets the Placeholder Texture.
        /// </summary>
        public Texture2D Placeholder
        { get { return moPlaceholderTexture; } }
        /// <summary>
        /// Gets the Particle Texture
        /// </summary>
        public Texture2D Particle
        { get { return moParticleTexture; } }

        //
        /* 
         * CONSTRUCTORS
         */
        //

        /// <summary>
        /// Default Constructor,
        /// will initialize our instance variables.
        /// </summary>
        public TextureCollection()
        { }

        //
        /*
         * PUBLIC METHODS
         */
        //

        /// <summary>
        /// Will load in all the external texture files
        /// </summary>
        /// <param name="loAssetsManager">
        /// Passed Assets Manager for our content pipeline
        /// </param>
        public void LoadContent(ContentManager loAssetsManager)
        {
            moEmptySquare = loAssetsManager.Load<Texture2D>(FilePaths.SquareEmptyTexture);
            moWallSquare = loAssetsManager.Load<Texture2D>(FilePaths.SquareWallTexture);
            moExitSquare = loAssetsManager.Load<Texture2D>(FilePaths.SquareExitTexture);
            moPlayerSquare = loAssetsManager.Load<Texture2D>(FilePaths.SquarePlayerTexture);
            moPlayerEyeSquare = loAssetsManager.Load<Texture2D>(FilePaths.SquarePlayerEyeTexture);
            moEnemyBasicSquare = loAssetsManager.Load<Texture2D>(FilePaths.SquareEnemyBasicTexture);
            moEnemyTurretSquare = loAssetsManager.Load<Texture2D>(FilePaths.SquareEnemyTurretTexture);
            moEnemyTurretEye = loAssetsManager.Load<Texture2D>(FilePaths.SquareEnemyTurretEyeTexture);
            moEnemyTurretBullet = loAssetsManager.Load<Texture2D>(FilePaths.SquareEnemyTurretBulletTexture);
            moButtonActiveTexture = loAssetsManager.Load<Texture2D>(FilePaths.ButtonActive);
            moButtonDeactiveTexture = loAssetsManager.Load<Texture2D>(FilePaths.ButtonDeactive);
            moButtonHoverTexture = loAssetsManager.Load<Texture2D>(FilePaths.ButtonHover);
            moButtonPressTexture = loAssetsManager.Load<Texture2D>(FilePaths.ButtonPress);
            moMainMenuTexture = loAssetsManager.Load<Texture2D>(FilePaths.MainMenuTexture);
            moPlaceholderTexture = loAssetsManager.Load<Texture2D>(FilePaths.MenuTitlePlaceholder);
            moParticleTexture = loAssetsManager.Load<Texture2D>(FilePaths.SquareParticleTexture);
        }

    }
} // End of namespace