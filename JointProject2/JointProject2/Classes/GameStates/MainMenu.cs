﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

/* Class Description
 *  -   Will handle and contain all our main menu objects,
 *      namely the buttons and all update logic associated to them.
 */

namespace JointProject2
{
    class MainMenu
    {
        //
        /*
         * VARIABLES & PROPERTIES
         */
        /**/

        // Rectangles to be used with placeholders
        private Button
            moButtonStartGame,
            moButtonContinueGame,
            moButtonOptions,
            moButtonExitGame,
            moButtonHelpGame;
        /// <summary>
        /// Gets the Button StartGame
        /// </summary>
        public Button ButtonStartGame
        { get { return moButtonStartGame; } }
        /// <summary>
        /// Gets the Button ContinueGame
        /// </summary>
        public Button ButtonContinueGame
        { get { return moButtonContinueGame; } }
        /// <summary>
        /// Gets the Button Options
        /// </summary>
        public Button ButtonOptions
        { get { return moButtonOptions; } }
        /// <summary>
        /// Gets the Button ExitGame
        /// </summary>
        public Button ButtonExitGame
        { get { return moButtonExitGame; } }
        /// <summary>
        /// Gets the Button HelpGame
        /// </summary>
        public Button ButtonHelpGame
        { get { return moButtonHelpGame; } }
        private MouseState moMouseState, moMouseOldState;
        private bool mdButtonPressed;


        //
        /*
         * CONSTANT DECLARATION
         */
        //

        // Main Menu Window Dimensions
        private const int MainMenuWidth = 800;
        /// <summary>
        /// Gets the Main Menu's window Width
        /// </summary>
        public int Width
        { get { return MainMenuWidth; } }
        private const int MainMenuHeight = 600;
        /// <summary>
        /// Gets the Main Menu's window Height
        /// </summary>
        public int Height
        { get { return MainMenuHeight; } }

        // Colour Constants
        private readonly Color TextColour = Color.WhiteSmoke;

        // Text Constants
        private const string TitleText =
            "Amazing Squares Arcade Game";
        /* BUTTON CONSTANTS */
        // Button Dimensions Constant
        private const int ButtonWidth = 180;
        private const int ButtonHeight = 60;
        private readonly Vector2 ButtonDimensions = new Vector2(ButtonWidth, ButtonHeight);
        // Button Offset Constant
        private const int ButtonOffsetX = 150;
        // Button StartGame Constants
        private const int ButtonStartGameX = ButtonOffsetX;
        private const int ButtonStartGameY = 200;
        private readonly Vector2 ButtonStartGamePosition = new Vector2(ButtonStartGameX, ButtonStartGameY);
        private const string ButtonStartGameText = "Start Game";
        // Button ContinueGame Constants
        private const int ButtonContinueGameX = ButtonOffsetX;
        private const int ButtonContinueGameY = 300;
        private readonly Vector2 ButtonContinueGamePosition = new Vector2(ButtonContinueGameX, ButtonContinueGameY);
        private const string ButtonContinueGameText = "Continue Game";
        // Button Options Constants
        private const int ButtonOptionsX = MainMenuWidth - ButtonOffsetX - ButtonWidth;
        private const int ButtonOptionsY = 200;
        private readonly Vector2 ButtonOptionsPosition = new Vector2(ButtonOptionsX, ButtonOptionsY);
        private const string ButtonOptionsText = "Options";
        // Button ExitGame Constants
        private const int ButtonExitGameX = MainMenuWidth - ButtonOffsetX - ButtonWidth;
        private const int ButtonExitGameY = 300;
        private readonly Vector2 ButtonExitGamePosition = new Vector2(ButtonExitGameX, ButtonExitGameY);
        private const string ButtonExitGameText = "Exit Game";
        // Button HelpGame Constants
        private const int ButtonHelpWidth = 500;
        private const int ButtonHelpGameX = 200;
        private const int ButtonHelpGameY = 400;
        private readonly Vector2 ButtonHelpGamePosition = new Vector2(ButtonHelpGameX, ButtonHelpGameY);
        private readonly Vector2 ButtonHelpDimensions = new Vector2(ButtonHelpWidth, ButtonHeight);
        private const string ButtonHelpGameText = "Controls: Arrow Keys to move, spacebar to jump";
        // Save Game extension
        private const string SaveGameExtension = ".save";

        //
        /*
         * CONSTRUCTOR
         */
        //

        /// <summary>
        /// Default Constructor,
        /// Intializes all object variables
        /// </summary>
        public MainMenu()
        {
            moButtonStartGame = new Button();
            moButtonContinueGame = new Button();
            moButtonOptions = new Button();
            moButtonExitGame = new Button();
            mdButtonPressed = false;
        }
        /// <summary>
        /// Constructor:
        /// Initializes all buttons,
        /// using the passed SpriteFont.
        /// </summary>
        /// <param name="loSpriteFont">
        /// Passed SpriteFont that will define what the text will be drawn with.
        /// </param>
        public MainMenu(SpriteFont loSpriteFont)
        {
            moButtonStartGame = new Button(ButtonStartGamePosition, ButtonDimensions, loSpriteFont, TextColour, ButtonStartGameText);
            moButtonContinueGame = new Button(ButtonContinueGamePosition, ButtonDimensions, loSpriteFont, TextColour, ButtonContinueGameText);
            moButtonOptions = new Button(ButtonOptionsPosition, ButtonDimensions, loSpriteFont, TextColour, ButtonOptionsText);
            moButtonExitGame = new Button(ButtonExitGamePosition, ButtonDimensions, loSpriteFont, TextColour, ButtonExitGameText);
            moButtonHelpGame = new Button(ButtonHelpGamePosition, ButtonHelpDimensions, loSpriteFont, TextColour, ButtonHelpGameText);
            mdButtonPressed = false;
        }

        //
        /*
         * PUBLIC METHODS
         */
        //

        /// <summary>
        /// Will handle Main Menu update logic
        /// </summary>
        /// <param name="leGameState">
        /// Referenced GameState variable.
        /// </param>
        /// <param name="ldRunOnce">
        /// Referenced boolean for Game State handling.
        /// </param>
        public void Update(ref Game.GameState leGameState, ref bool ldRunOnce, ref bool ldSaveGameLoaded, ref int ldLevel, string ldPlayerName)
        {
            moButtonStartGame.Update();
            moButtonContinueGame.Update();
            moButtonOptions.Update();
            moButtonExitGame.Update();

            if (mdButtonPressed == false)
            {
                RefreshMouse();
                ComputeCollisions(moButtonStartGame);
                if (File.Exists(FilePaths.SaveGameFolder + ldPlayerName + SaveGameExtension) == true)
                {
                    moButtonContinueGame.SetActive();
                    ComputeCollisions(moButtonContinueGame);
                }
                else
                {
                    moButtonContinueGame.SetDeactive();
                }
                ComputeCollisions(moButtonOptions);
                ComputeCollisions(moButtonExitGame);
            }
            else // mdButtonPressed == true
            {
                if (moButtonOptions.IsBeingPressed() == false)
                {
                    MediaPlayer.Volume *= 0.7f;
                }
                if (moButtonStartGame.IsPressed() == true)
                {
                    leGameState = Game.GameState.InGame;
                    ldRunOnce = true;
                    ldLevel = 1;
                    ldSaveGameLoaded = false;
                    mdButtonPressed = false;
                }
                else if (moButtonContinueGame.IsPressed() == true)
                {
                    leGameState = Game.GameState.Loading;
                    ldRunOnce = true;
                    mdButtonPressed = false;
                }
                else if (moButtonExitGame.IsPressed() == true)
                {
                    leGameState = Game.GameState.Exit;
                    mdButtonPressed = false;
                }
                else if (moButtonOptions.IsPressed() == true)
                {
                    leGameState = Game.GameState.Options;
                    mdButtonPressed = false;
                }
            }
        }

        //
        /* 
         * PRIVATE METHODS
         */
        //

        /// <summary>
        /// Updates our Mouse states method call.
        /// </summary>
        private void RefreshMouse()
        {
            moMouseOldState = moMouseState;
            moMouseState = Mouse.GetState();
        }
        /// <summary>
        /// Will Compute all mouse Collisions with the Passed Button
        /// and set the button to its appropriate state,
        /// only if no other button has been pressed.
        /// </summary>
        /// <param name="loButton">
        /// Passed Button that will be checked against the mouse position.
        /// </param>
        private void ComputeCollisions(Button loButton)
        {
            if (loButton.IsActive() == true)
            {
                if (loButton.IsBeingPressed() == false)
                {
                    if (CheckIfIn(moMouseState.X, moMouseState.Y, loButton.Bounds) == true)
                    {
                        if (moMouseState.LeftButton == ButtonState.Pressed &&
                            moMouseOldState.LeftButton == ButtonState.Released)
                        {
                            loButton.Pressed();
                            mdButtonPressed = true;
                        }
                        else if (loButton.State == Button.ButtonState.Active)
                        {
                            loButton.Hovered();
                        }
                    }
                    else
                    {
                        loButton.SetActive();
                    }
                }
            }
        }
        /// <summary>
        /// Will check,
        /// using the two Passed integer's as coordinates (x and y, respectively),
        /// if they are inside the Passed Rectangle.
        /// </summary>
        /// <param name="ldXCoord">
        /// Passed integer to be used as a X coordinate.
        /// </param>
        /// <param name="ldYCoord">
        /// Passed integer to be used as a Y coordinate.
        /// </param>
        /// <param name="loBoundingRect">
        /// Passed Rectangle to be checked if other two int's are inside Passed Rectangle.
        /// </param>
        /// <returns>
        /// Returns true if the Passed coordinates are inside the Rectangle,
        /// else false.
        /// </returns>
        private bool CheckIfIn(int ldXCoord, int ldYCoord, Rectangle loBoundingRect)
        {
            bool ldInside = false;
            if (ldXCoord > loBoundingRect.X &&
                ldXCoord < loBoundingRect.X + loBoundingRect.Width &&
                ldYCoord > loBoundingRect.Y &&
                ldYCoord < loBoundingRect.Y + loBoundingRect.Height)
            {
                ldInside = true;
            }
            return ldInside;
        }

    }
} // End of namespace