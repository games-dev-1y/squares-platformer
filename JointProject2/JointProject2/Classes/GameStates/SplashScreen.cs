﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

/* Class Description
 *  Will display a background texture and wait for user to press a key
 */

namespace JointProject2
{
    class SplashScreen
    {
        //
        /*
         * VARIABLES & PROPERTIES
         */
        /**/

        // Splash Screen Sounds
        Song moBackgroundSong;
        // Background image texture
        private Texture2D moForeground, moBackground, moSelectionBar;
        // Background image moPosition (used in draw)
        private Vector2 moPosition, moNamePosition;
        // Text Placeholder Texture
        private Texture2D moPlaceholderTexture;
        private Texture2D moSelectionPlaceholderTexture;
        // Text Placeholder
        private Rectangle moTextPlaceholder;
        private Rectangle moTitlePlaceholder;
        private Rectangle moAskNamePlaceholder;
        private Rectangle moNamePlaceholder;
        // Used to write text
        private SpriteFont moSpriteFont;
        private SpriteFont moTitleFont;
        // For the fading effect
        private Color moColour;
        private int mdFadeAlphaValue;
        private double mdFadeDelay;
        // Screen Counters
        private double mdCounter;
        // Keyboard States
        private KeyboardState moKeyboardState, moKeyboardOldState;
        // String to be set as the player's name
        private string mdPlayerName;
        /// <summary>
        /// Gets the name entered by the player.
        /// </summary>
        public string PlayerName
        { get { return mdPlayerName; } }
        // Splash Screen States
        private enum ScreenStates
        { Normal, AskName, FadeOut, FadeIn, CountOut }
        private ScreenStates meScreenStates;
        // For AskName Screen State
        private bool mdCapslock;
        private bool mdDrawSelectionBar, mdDrawSelection;
        private int mdCurrentSelection;
        private Vector2 moSelectionPosition;

        //
        /*
         * CONSTANT DECLARATION
         */
        //

        // Splash Screen Window Dimensions
        private const int SplashScreenWidth = 800;
        /// <summary>
        /// Gets the Splash Screen's window Width
        /// </summary>
        public int Width
        { get { return SplashScreenWidth; } }
        private const int SplashScreenHeight = 600;
        /// <summary>
        /// Gets the Splash Screen's window Height
        /// </summary>
        public int Height
        { get { return SplashScreenHeight; } }
        private readonly Vector2 CenterPosition = new Vector2(SplashScreenWidth / 2f, SplashScreenHeight / 2f);
        /// <summary>
        /// Gets the Splash Screen's Center Position.
        /// </summary>
        public Vector2 Center
        { get { return CenterPosition; } }
        
        // Fading effect Constant
        private const double FadeDelay = 0.05;
        private const byte FadingRate = 10;

        // Counter Constants
        private const double AmountOfSecs = 0.5;
        private const int BlinkTimeOn = 40;
        private const int BlinkTimeOff = 10;

        private const int MaxPlayerName = 20;

        // No Filter
        private readonly Color NoFilter = Color.White;

        // Text Constants
        private const string PressToContinueText =
            "Press Any Key to Continue";
        private const string TitleText =
            "Amazing Squares Arcade Game";
        private const string AskNameText =
            "Enter Name:";

        // Text Positions
        private const float PressToContinueX = 300f;
        private const float PressToContinueY = 400f;
        private const float AskNameX = 340f;
        private const float AskNameY = 200f;
        private const float TitleX = 120f;
        private const float TitleY = 100f;
        // Text Bounding Box Offsets
        private const float TitleOffsetX = 40f;
        private const float TitleOffsetY = 0f;
        private const float TextOffsetX = 20f;
        private const float TextOffsetY = 0f;

        // Text Placeholder Dimensions
        private const int PlaceholderTitleWidth = 700;
        private const int PlaceholderTitleHeight = 60;
        private const int PlaceholderTextWidth = 320;
        private const int PlaceholderTextHeight = 30;
        private const int PlaceholderAskNameWidth = 150;
        private const int PlaceholderAskNameHeight = 30;

        // Text Vectors using Text Positions
        private readonly Vector2 PressToContinuePos =
            new Vector2(PressToContinueX, PressToContinueY);
        private readonly Vector2 TitlePosition =
            new Vector2(TitleX, TitleY);
        private readonly Vector2 AskNamePosition =
            new Vector2(AskNameX, AskNameY);
        // Vector Offset for Text Boundary Boxs
        private readonly Vector2 TitleOffset =
            new Vector2(TitleOffsetX, TitleOffsetY);
        private readonly Vector2 TextOffset =
            new Vector2(TextOffsetX, TextOffsetY);
        private readonly Vector2 PlaceholderOffset =
            new Vector2(2f, 2f);

        //
        /*
         * CONSTRUCTOR
         */
        //

        /// <summary>
        /// Default Constructor,
        /// Intializes all object variables
        /// </summary>
        public SplashScreen()
        {
            moPosition = new Vector2(0f);
            moForeground = null;
            moPlaceholderTexture = null;
            moTitlePlaceholder = new Rectangle(
                (int)(TitlePosition.X - TitleOffset.X),
                (int)(TitlePosition.Y - TitleOffset.Y),
                PlaceholderTitleWidth,
                PlaceholderTitleHeight
                );
            moTextPlaceholder = new Rectangle(
                (int)(PressToContinuePos.X - TextOffset.X),
                (int)(PressToContinuePos.Y - TextOffset.Y),
                PlaceholderTextWidth,
                PlaceholderTextHeight
                );
            moAskNamePlaceholder = new Rectangle(
                (int)(AskNamePosition.X - TextOffsetX),
                (int)(AskNamePosition.Y - TextOffsetY),
                PlaceholderAskNameWidth,
                PlaceholderAskNameHeight
                );
            moNamePlaceholder = new Rectangle();
            moNamePosition = new Vector2(0f);
            moSelectionPosition = new Vector2(0f);
            mdPlayerName = "";
            mdFadeDelay = FadeDelay;
            mdFadeAlphaValue = 0;
            moColour = Color.White;
            meScreenStates = ScreenStates.FadeIn;
            mdCapslock = false;
            mdDrawSelectionBar = false;
            mdDrawSelection = false;
            mdCurrentSelection = 0;
            MediaPlayer.IsRepeating = true;
        }

        

        //
        /*
         * PUBLIC METHODS
         */
        //

        /// <summary>
        /// Will load splash screen content
        /// </summary>
        /// <param name="loAssetsManager">
        /// Assets Manager for our content pipeline
        /// </param>
        public void LoadContent(ContentManager loAssetsManager)
        {
            moBackground = loAssetsManager.Load<Texture2D>(FilePaths.SplashScreenBackgroundTexture);
            moForeground = loAssetsManager.Load<Texture2D>(FilePaths.SplashScreenForegroundTexture);
            moPlaceholderTexture = loAssetsManager.Load<Texture2D>(FilePaths.SplashScreenPlaceholder);
            moSelectionBar = loAssetsManager.Load<Texture2D>(FilePaths.SplashScreenSelectionBarTexture);
            moSpriteFont = loAssetsManager.Load<SpriteFont>(FilePaths.SplashScreenFont);
            moTitleFont = loAssetsManager.Load<SpriteFont>(FilePaths.SplashScreenTitleFont);
            moSelectionPlaceholderTexture = loAssetsManager.Load<Texture2D>(FilePaths.SplashScreenSelectionPlaceholderTexture);
            moBackgroundSong = loAssetsManager.Load<Song>(FilePaths.SoundSplashScreenSoundtrack);
            MediaPlayer.Play(moBackgroundSong);
        }
        /// <summary>
        /// Will handle splash screen update logic
        /// </summary>
        public void Update(ref Game.GameState leGameState, ref bool ldRunOnce, ref GameTime loGameTime)
        {
            switch (meScreenStates)
            {
                case ScreenStates.CountOut:
                    CountOutUpdate(ref leGameState, ref ldRunOnce, ref loGameTime);
                    break;
                case ScreenStates.FadeOut:
                    FadeOutUpdate(ref loGameTime);
                    break;
                case ScreenStates.FadeIn:
                    FadeInUpdate(ref loGameTime);
                    break;
                case ScreenStates.AskName:
                    AskNameUpdate();
                    break;
                case ScreenStates.Normal:
                default:
                    NormalUpdate();
                    break;
            }
        }
        /// <summary>
        /// Will draw the splash screen
        /// </summary>
        /// <param name="loSpriteBatch"></param>
        public void Draw(SpriteBatch loSpriteBatch)
        {
            loSpriteBatch.Draw(moBackground, moPosition, NoFilter);
            loSpriteBatch.Draw(moForeground, moPosition, moColour);
            // Drawing Title
            loSpriteBatch.Draw(moPlaceholderTexture, moTitlePlaceholder, moColour); // Placeholder
            loSpriteBatch.DrawString(moTitleFont, TitleText, TitlePosition, moColour);
            switch (meScreenStates)
            {
                case ScreenStates.FadeOut:
                case ScreenStates.CountOut:
                case ScreenStates.AskName:
                    loSpriteBatch.Draw(moPlaceholderTexture, moAskNamePlaceholder, moColour);
                    if (mdDrawSelection == true)
                    {
                        loSpriteBatch.Draw(moSelectionPlaceholderTexture, moNamePlaceholder, moColour);
                    }
                    loSpriteBatch.DrawString(moSpriteFont, AskNameText, AskNamePosition, moColour);
                    loSpriteBatch.DrawString(moSpriteFont, mdPlayerName, moNamePosition, moColour);
                    if (mdDrawSelectionBar == true)
                    {
                        loSpriteBatch.Draw(moSelectionBar, moSelectionPosition, moColour);
                    }
                    break;
                case ScreenStates.FadeIn:
                case ScreenStates.Normal:// Drawing Text
                    loSpriteBatch.Draw(moPlaceholderTexture, moTextPlaceholder, moColour); // Placeholder
                    loSpriteBatch.DrawString(moSpriteFont, PressToContinueText, PressToContinuePos, moColour);
                    break;
                default:
                    break;
            }
        }

        //
        /* 
         * PRIVATE METHODS
         */
        //

        /// <summary>
        /// Handles the Update when in the CountOut ScreenState.
        /// </summary>
        private void CountOutUpdate(ref Game.GameState leGameState, ref bool ldRunOnce, ref GameTime loGameTime)
        {
            mdCounter += loGameTime.ElapsedGameTime.TotalSeconds;
            if (mdCounter >= AmountOfSecs)
            {
                Exit(ref leGameState, ref ldRunOnce);
            }
        }
        /// <summary>
        /// Handles the Update when in the FadeOut ScreenState.
        /// </summary>
        private void FadeOutUpdate(ref GameTime loGameTime)
        {
            mdFadeDelay -= loGameTime.ElapsedGameTime.TotalSeconds;
            if (mdFadeDelay < 0)
            {
                mdFadeDelay = FadeDelay;
                MediaPlayer.Volume *= 0.8f;
                mdFadeAlphaValue -= FadingRate;
                if (mdFadeAlphaValue >= 255)
                {
                    mdFadeAlphaValue = 255;
                }
                else if (mdFadeAlphaValue <= 0)
                {
                    mdFadeAlphaValue = 0;
                }
                else
                {
                    moColour.A = (byte)mdFadeAlphaValue;
                }
            }
            if (mdFadeAlphaValue == 0)
            {
                MediaPlayer.Volume = 0;
                meScreenStates = ScreenStates.CountOut;
            }
        }
        /// <summary>
        /// Handles the Update when in the FadeIn ScreenState.
        /// </summary>
        private void FadeInUpdate(ref GameTime loGameTime)
        {
            mdFadeDelay -= loGameTime.ElapsedGameTime.TotalSeconds;
            if (mdFadeDelay < 0)
            {
                mdFadeDelay = FadeDelay;
                mdFadeAlphaValue += FadingRate;
                if (mdFadeAlphaValue >= 255)
                {
                    mdFadeAlphaValue = 255;
                }
                else if (mdFadeAlphaValue <= 0)
                {
                    mdFadeAlphaValue = 0;
                }
                else
                {
                    moColour.A = (byte)mdFadeAlphaValue;
                }
            }
            if (mdFadeAlphaValue == 255)
            {
                meScreenStates = ScreenStates.Normal;
            }
        }
        /// <summary>
        /// Handles the Update when in the AskName ScreenState.
        /// </summary>
        private void AskNameUpdate()
        {
            mdCounter++;
            Vector2 loNamePosition = moSpriteFont.MeasureString(mdPlayerName);
            moNamePosition.X = this.Center.X - (loNamePosition.X / 2f);
            moNamePosition.Y = this.Center.Y - (loNamePosition.Y / 2f);
            moNamePlaceholder.X = (int)(moNamePosition.X - PlaceholderOffset.X);
            moNamePlaceholder.Y = (int)(moNamePosition.Y - PlaceholderOffset.Y);
            moNamePlaceholder.Width = (int)(loNamePosition.X + (PlaceholderOffset.X * 2));
            moNamePlaceholder.Height = (int)(loNamePosition.Y + (PlaceholderOffset.Y * 2));

            if (mdPlayerName.Length > 0)
            {
                mdDrawSelection = true;
                moSelectionPosition.X =
                    moNamePosition.X + moSpriteFont.MeasureString(mdPlayerName.Substring(0, mdCurrentSelection)).X;
                moSelectionPosition.Y = moNamePosition.Y;
            }
            else
            {
                mdDrawSelection = false;
                mdDrawSelectionBar = false;
                mdCounter = 0;
                moSelectionPosition.X = moNamePosition.X;
                moSelectionPosition.Y = moNamePosition.Y - moSelectionBar.Height;
            }
            if (mdDrawSelectionBar == true)
            {
                if (mdCounter > BlinkTimeOn)
                {
                    mdCounter = 0;
                    mdDrawSelectionBar = false;
                }
            }
            else if (mdDrawSelectionBar == false)
            {
                if (mdCounter > BlinkTimeOff)
                {
                    mdCounter = 0;
                    mdDrawSelectionBar = true;
                }
            }
            RefreshKeyboard();
            Input();
        }
        /// <summary>
        /// Handles the Update when in the Normal ScreenState.
        /// </summary>
        private void NormalUpdate()
        {
            if (Keyboard.GetState().GetPressedKeys().Length > 0)
            {
                meScreenStates = ScreenStates.AskName;
            }
        }
        /// <summary>
        /// Will exit the SplashScreen,
        /// set the run once boolean to true,
        /// and move the GameState to the MainMenu State.
        /// </summary>
        /// <param name="leGameState">
        /// Passed Game.GameState enum which controls,
        /// our Game's different states.
        /// </param>
        /// <param name="ldRunOnce">
        /// Passed Boolean that allows for our Game to run certain content only once.
        /// </param>
        private void Exit(ref Game.GameState leGameState, ref bool ldRunOnce)
        {
            meScreenStates = ScreenStates.FadeIn;
            leGameState = Game.GameState.MainMenu;
            ldRunOnce = true;
            MediaPlayer.Stop();
            MediaPlayer.Volume = 1f;
        }
        /// <summary>
        /// Refreshes the current Keyboard input
        /// </summary>
        private void RefreshKeyboard()
        {
            moKeyboardOldState = moKeyboardState;
            moKeyboardState = Keyboard.GetState();
        }
        /// <summary>
        /// Will handle all input logic,
        /// and iterate through all the keys pressed.
        /// </summary>
        private void Input()
        {
            if (moKeyboardState.GetPressedKeys().Length > 0)
            {
                Keys[] laPressedKeys = moKeyboardState.GetPressedKeys(),
                laOldPressedKeys = moKeyboardOldState.GetPressedKeys();
                for (int i = 0; i < laPressedKeys.Length; i++)
                {
                    if (laOldPressedKeys.Length > i)
                    {
                        if (laOldPressedKeys[i] != laPressedKeys[i])
                        {
                            VerifyInput(laPressedKeys[i]);
                        }
                    }
                    else
                    {
                        VerifyInput(laPressedKeys[i]);
                    }
                    
                }
            }
            if (mdPlayerName.Length > MaxPlayerName)
            {
                mdPlayerName = mdPlayerName.Remove(mdPlayerName.Length - 1);
                mdCurrentSelection--;
            }
        }
        /// <summary>
        /// Will verify passed key,
        /// and proccess key executing certain commands based on the passed key.
        /// </summary>
        /// <param name="leKeyPressed">
        /// Passed Keys will determine what command will be executed.
        /// </param>
        private void VerifyInput(Keys leKeyPressed)
        {
            if (leKeyPressed == Keys.Enter && mdPlayerName.Length > 0)
            {
                meScreenStates = ScreenStates.FadeOut;
            }
            else if (leKeyPressed == Keys.Back)
            {
                if (mdCurrentSelection > 0)
                {
                    string ldPlayerName = mdPlayerName.Substring(mdCurrentSelection);
                    mdPlayerName = mdPlayerName.Remove(mdCurrentSelection - 1);
                    mdPlayerName += ldPlayerName;
                    mdCurrentSelection--;
                }
            }
            else if (leKeyPressed == Keys.Delete)
            {
                if (mdCurrentSelection >= 0 && mdCurrentSelection < mdPlayerName.Length)
                {
                    string ldPlayerName = mdPlayerName.Substring(mdCurrentSelection + 1);
                    mdPlayerName = mdPlayerName.Remove(mdCurrentSelection);
                    mdPlayerName += ldPlayerName;
                }
            }
            else if (leKeyPressed == Keys.Left)
            {
                if (mdCurrentSelection > 0)
                {
                    mdCurrentSelection--;
                }
            }
            else if (leKeyPressed == Keys.Right)
            {
                if (mdCurrentSelection < mdPlayerName.Length)
                {
                    mdCurrentSelection++;
                }
            }
            else
            {
                if (mdCurrentSelection < mdPlayerName.Length)
                {
                    mdPlayerName = mdPlayerName.Insert(mdCurrentSelection, DecideKey(leKeyPressed));
                }
                else
                {
                    mdPlayerName += DecideKey(leKeyPressed);
                }
            }
        }
        /// <summary>
        /// Will decide what string to return based on what passed
        /// </summary>
        /// <param name="leKeyPressed">
        /// Passed Keys, determines what string it returns.
        /// </param>
        /// <returns>
        /// Returns a string of the Pressed Key
        /// (only valid keys are converted,
        /// for example: no PageUp key is ignored)
        /// </returns>
        private string DecideKey(Keys leKeyPressed)
        {
            string ldKeyString = "";
            bool ldShiftPressed;

            switch (leKeyPressed)
            {
                case Keys.CapsLock:
                    mdCapslock = !mdCapslock;
                    break;
                case Keys.Space:
                    ldKeyString = " ";
                    mdCurrentSelection++;
                    break;
                case Keys.D0:
                case Keys.D1:
                case Keys.D2:
                case Keys.D3:
                case Keys.D4:
                case Keys.D5:
                case Keys.D6:
                case Keys.D7:
                case Keys.D8:
                case Keys.D9:
                    ldKeyString = leKeyPressed.ToString().Substring(1);
                    mdCurrentSelection++;
                    break;
                case Keys.NumPad0:
                case Keys.NumPad1:
                case Keys.NumPad2:
                case Keys.NumPad3:
                case Keys.NumPad4:
                case Keys.NumPad5:
                case Keys.NumPad6:
                case Keys.NumPad7:
                case Keys.NumPad8:
                case Keys.NumPad9:
                    ldKeyString = leKeyPressed.ToString().Substring(6);
                    mdCurrentSelection++;
                    break;
                case Keys.A:
                case Keys.B:
                case Keys.C:
                case Keys.D:
                case Keys.E:
                case Keys.F:
                case Keys.G:
                case Keys.H:
                case Keys.I:
                case Keys.J:
                case Keys.K:
                case Keys.L:
                case Keys.M:
                case Keys.N:
                case Keys.O:
                case Keys.P:
                case Keys.Q:
                case Keys.R:
                case Keys.S:
                case Keys.T:
                case Keys.U:
                case Keys.V:
                case Keys.W:
                case Keys.X:
                case Keys.Y:
                case Keys.Z:
                    ldShiftPressed = (moKeyboardState.IsKeyDown(Keys.LeftShift) == true ||
                        moKeyboardState.IsKeyDown(Keys.RightShift) == true);
                    if (
                        (ldShiftPressed == true && mdCapslock == false) ||
                        (mdCapslock == true && ldShiftPressed == false)
                        )
                    {
                        ldKeyString = (leKeyPressed.ToString()).ToUpper();
                        mdCurrentSelection++;
                    }
                    else
                    {
                        ldKeyString = (leKeyPressed.ToString()).ToLower();
                        mdCurrentSelection++;
                    }
                    break;
                default:
                    break;
            }
            return ldKeyString;
        }

    }
} // End of namespace