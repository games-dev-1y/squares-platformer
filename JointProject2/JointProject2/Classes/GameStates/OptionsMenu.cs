﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

/* Class Description
 *  -   Will handle and contain all our menu's objects,
 *      namely the buttons and all update logic associated to them.
 */

namespace JointProject2
{
    class OptionsMenu
    {

        //
        /*
         * VARIABLES & PROPERTIES
         */
        //

        // Our own button class declarations
        private Button
            moBtnBack,
            moBtnMute,
            moBtnRename;
        /// <summary>
        /// Gets the Back Button
        /// </summary>
        public Button BtnBack
        { get { return moBtnBack; } }
        /// <summary>
        /// Gets the Mute Button
        /// </summary>
        public Button BtnMute
        { get { return moBtnMute; } }
        /// <summary>
        /// Gets the RenameButton
        /// </summary>
        public Button BtnRename
        { get { return moBtnRename; } }
        private MouseState moMouseState, moMouseOldState;
        private SpriteFont moBtnFont;
        private bool mdButtonPressed;

        //
        /* 
         * CONSTANT DECLARATION
         */
        //
        
        // Options Menu Window Dimensions
        private const int OptionsMenuWidth = 800;
        /// <summary>
        /// Gets the Options Menu's window width
        /// </summary>
        public int Width
        { get { return OptionsMenuWidth; } }
        private const int OptionsMenuHeight = 600;
        /// <summary>
        /// Gets the Options Menu's window height
        /// </summary>
        public int Height
        { get { return OptionsMenuHeight; } }

        // Colour constants
        private readonly Color NoFilter = Color.White;
        private readonly Color TextColour = Color.WhiteSmoke;

        // Text Constants
        private const string TitleText =
            "Amazing Squares Arcade Game";
        /* BUTTON CONSTANTS */
        // Button Dimension Constants
        private const int ButtonWidth = 250;
        private const int ButtonHeight = 80;
        private readonly Vector2 ButtonDimensions = new Vector2(ButtonWidth, ButtonHeight);
        // Button Back Constants
        private const int ButtonBackX = 20;
        private const int ButtonBackY = 20;
        private readonly Vector2 ButtonBackPosition = new Vector2(ButtonBackX, ButtonBackY);
        private const string ButtonBackText = "Back";
        // Button Mute constant
        private const int ButtonMuteX = 150;
        private const int ButtonMuteY = 300;
        private readonly Vector2 ButtonMutePosition = new Vector2(ButtonMuteX, ButtonMuteY);
        private const string ButtonMuteText = ButtonMuteOffText;
        private const string ButtonMuteOnText = "Mute: On";
        private const string ButtonMuteOffText = "Mute: Off";
        // Button Rename constant
        private const int ButtonRenameX = 450;
        private const int ButtonRenameY = 300;
        private readonly Vector2 ButtonRenamePosition = new Vector2(ButtonRenameX, ButtonRenameY);
        private const string ButtonRenameText = "Change Name";

        //
        /* 
         * CONSTRUCTOR
         */
        //

        /// <summary>
        /// Default Constructor:
        /// Initializes all object variables
        /// </summary>
        public OptionsMenu()
        {
            mdButtonPressed = false;
        }

        //
        /* 
         * PUBLIC METHODS
         */
        //

        /// <summary>
        /// Will load the spritefont into all the buttons
        /// </summary>
        /// <param name="aoSpriteFont"></param>
        public void LoadContent(SpriteFont aoSpriteFont)
        {
            moBtnFont = aoSpriteFont;
            moBtnBack = new Button(
                ButtonBackPosition, ButtonDimensions, moBtnFont, TextColour, ButtonBackText
                );
            moBtnMute = new Button(
                ButtonMutePosition, ButtonDimensions, moBtnFont, TextColour, ButtonMuteText
                );
            moBtnRename = new Button(
                ButtonRenamePosition, ButtonDimensions, moBtnFont, TextColour, ButtonRenameText
                );
        }
        /// <summary>
        /// Will handle Options Menu update logic
        /// </summary>
        /// <param name="leGameState"></param>
        public void Update(ref Game.GameState leGameState)
        {
            moBtnBack.Update();
            moBtnMute.Update();
            moBtnRename.Update();

            if (mdButtonPressed == false)
            {
                RefreshMouse();
                ComputeCollisions(moBtnBack);
                ComputeCollisions(moBtnMute);
                ComputeCollisions(moBtnRename);
            }
            else // mdButtonPressed == true
            {
                if (moBtnBack.IsPressed() == true)
                {
                    leGameState = Game.GameState.MainMenu;
                    mdButtonPressed = false;
                }
                else if (moBtnMute.IsPressed() == true)
                {
                    MediaPlayer.IsMuted = !MediaPlayer.IsMuted;
                    if (MediaPlayer.IsMuted == true)
                    {
                        moBtnMute.SetText(ButtonMuteOnText);
                        MediaPlayer.Pause();
                    }
                    else
                    {
                        moBtnMute.SetText(ButtonMuteOffText);
                        MediaPlayer.Resume();
                    }
                    mdButtonPressed = false;
                }
                else if (moBtnRename.IsPressed() == true)
                {
                    leGameState = Game.GameState.SplashScreen;
                    mdButtonPressed = false;
                }
            }
        }

        //
        /*
         * PRIVATE METHODS
         */
        //

        /// <summary>
        /// Updates our Mouse states method call.
        /// </summary>
        private void RefreshMouse()
        {
            moMouseOldState = moMouseState;
            moMouseState = Mouse.GetState();
        }
        /// <summary>
        /// Will Compute all mouse Collisions with the Passed Button
        /// and set the button to its appropriate state,
        /// only if no other button has been pressed.
        /// </summary>
        /// <param name="loButton">
        /// Passed Button that will be checked against the mouse position.
        /// </param>
        private void ComputeCollisions(Button loButton)
        {
            if (loButton.IsActive() == true)
            {
                if (loButton.IsBeingPressed() == false)
                {
                    if (CheckIfIn(moMouseState.X, moMouseState.Y, loButton.Bounds) == true)
                    {
                        if (moMouseState.LeftButton == ButtonState.Pressed &&
                            moMouseOldState.LeftButton == ButtonState.Released)
                        {
                            loButton.Pressed();
                            mdButtonPressed = true;
                        }
                        else if (loButton.State == Button.ButtonState.Active)
                        {
                            loButton.Hovered();
                        }
                    }
                    else
                    {
                        loButton.SetActive();
                    }
                }
            }
        }
        /// <summary>
        /// Will check,
        /// using the two Passed integer's as coordinates (x and y, respectively),
        /// if they are inside the Passed Rectangle.
        /// </summary>
        /// <param name="ldXCoord">
        /// Passed integer to be used as a X coordinate.
        /// </param>
        /// <param name="ldYCoord">
        /// Passed integer to be used as a Y coordinate.
        /// </param>
        /// <param name="loBoundingRect">
        /// Passed Rectangle to be checked if other two int's are inside Passed Rectangle.
        /// </param>
        /// <returns>
        /// Returns true if the Passed coordinates are inside the Rectangle,
        /// else false.
        /// </returns>
        private bool CheckIfIn(int ldXCoord, int ldYCoord, Rectangle loBoundingRect)
        {
            bool ldInside = false;
            if (ldXCoord > loBoundingRect.X &&
                ldXCoord < loBoundingRect.X + loBoundingRect.Width &&
                ldYCoord > loBoundingRect.Y &&
                ldYCoord < loBoundingRect.Y + loBoundingRect.Height)
            {
                ldInside = true;
            }
            return ldInside;
        }

    }
} // End of namespace