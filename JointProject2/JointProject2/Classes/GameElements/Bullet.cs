﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

/* Class Description:
 * This is the Bullet class which,
 * represents a bullet.
 */

namespace JointProject2
{
    class Bullet
    {
        //
        /* 
         * VARIABLES & PROPERTIES
         */
        //

        /* Will store and alter the moPosition of the bullet */
        private Vector2 moPosition;
        /// <summary>
        /// Gets the moPosition vector of the bullet
        /// </summary>
        public Vector2 Position
        { get { return moPosition; } }

        // Will track the objects (the one firing this bullet) moPosition
        private Vector2 moShotOrigin;

        // Will store the direction and moVelocity the bullet is moving
        private Vector2 moVelocity;
        /// <summary>
        /// Gets the Velocity vector of the bullet.
        /// </summary>
        public Vector2 Velocity
        { get { return moVelocity; } }

        // Will be used to store the limitations of the bullet,
        // to be checked against the other sprites for collision
        private Rectangle moBoundaryBox;
        /// <summary>
        /// will return the boundary box for collisions
        /// </summary>
        public Rectangle BoundaryBox
        { get { return moBoundaryBox; } }

        // Will be used to store the center of the rotation of the bullet
        private Vector2 moRotationPosition;

        // Will be used to store the center loLocation of the bullet texture,
        // to be used in the Draw() method
        private Vector2 moCenterDrawPosition;
        /// <summary>
        /// Gets the center moPosition that the bullet rotates on
        /// </summary>
        public Vector2 CenterPosition
        { get { return moCenterDrawPosition; } }
        
        // Represents the rotation/direction in degrees the bullet is facing
        private float mdRotationAngle;
        /// <summary>
        /// Gets or sets the rotation of the bullet
        /// </summary>
        public float RotationAngle
        { get { return mdRotationAngle; } }
        /// <summary>
        /// Gets the rotation of the bullet in radians
        /// </summary>
        public float RotationAngleRad
        { get { return AngleToRadians(mdRotationAngle); } }

        // Used by the game class to determine whether the bullet,
        // is mdAlive or dead
        private bool mdAlive;
        /// <summary>
        /// returns whether bullet is mdAlive or not
        /// </summary>
        public bool Alive
        { get { return mdAlive; } }

        // Will track in what state the bullet is in
        private enum BulletState
        {
            Alive, Dead, Pause
        }
        private BulletState meBulletState;
        private BulletState meBulletPreviousState;

        // Speed of the bullet
        private int mdSpeed = 1;

        // Bullets position in the world array
        private int mdRow, mdCol;
        /// <summary>
        /// Gets the current Row that the bullet is in.
        /// </summary>
        public int Row
        { 
            get { return mdRow; }
            set { mdRow = value; }
        }
        /// <summary>
        /// Gets the current collumn that the bullet is in.
        /// </summary>
        public int Col
        {
            get { return mdCol; }
            set { mdCol = value; }
        }

        //
        /* 
         * CONSTANT DECLARATION
         */
        //

        // Dimensions of the bullet
        private const int ConsWidth = 6;
        /// <summary>
        /// Gets the bullets ConsWidth
        /// </summary>
        public int Width
        { get { return ConsWidth; } }
        private const int ConsHeight = 6;
        /// <summary>
        /// Gets the ConsHeight of the bullet
        /// </summary>
        public int Height
        { get { return ConsHeight; } }

        //
        /* 
         * CONSTRUCTORS
         */
        //

        /// <summary>
        /// Default Constructor
        /// </summary>
        public Bullet()
        {
            int x = -100, y = -100;
            moPosition = new Vector2(x, y);
            moBoundaryBox = new Rectangle(x, y, ConsWidth, ConsHeight);
            moCenterDrawPosition.X = ConsWidth / 2f;
            moCenterDrawPosition.Y = ConsHeight / 2f;
            moVelocity = new Vector2(0f);
            meBulletState = BulletState.Dead;
            mdRotationAngle = 0f;
        }

        //
        /* 
         * PUBLIC METHODS
         */
        //

        /// <summary>
        /// Will update based on the bullets state,
        /// Dead:   Will simply update its firing position;
        /// Alive:  Will move the bullet along its firing vector.
        /// </summary>
        /// <param name="loSpawnLocation">
        /// Passed Vector2,
        /// used for the bullet to track its initial firing position.
        /// </param>
        public void Update(Vector2 loSpawnLocation)
        {
            switch (meBulletState)
            {
                case BulletState.Pause:
                    break;
                case BulletState.Dead:
                    SpawnPoint(loSpawnLocation);
                    break;
                case BulletState.Alive:
                default:
                    Move();
                    break;
            }
            AliveTracker();
            LocationUpdate();
        }
        /// <summary>
        /// Fires in the angle passed as a argument,
        /// by setting it to mdAlive
        /// </summary>
        /// <param name="rotation">
        /// Passed float, sets the bullets angle of rotation.
        /// </param>
        public void Fire(float rotation)
        {
            meBulletState = BulletState.Alive;
            moShotOrigin = moPosition;
            mdRotationAngle = rotation;
        }
        /// <summary>
        /// Will make the bullet set to the dead state
        /// </summary>
        public void Die()
        {
            meBulletState = BulletState.Dead;
            moPosition = moShotOrigin;
            moVelocity = new Vector2(0f);
        }
        /// <summary>
        /// Will set the bullets row and collumn,
        /// based on the two passed integers.
        /// </summary>
        /// <param name="ldRow">
        /// Passed integer, that will be used to set the row.
        /// </param>
        /// <param name="ldCol">
        /// Passed integer, that will be used to set the collumn.
        /// </param>
        public void SetRowCollumn(int ldRow, int ldCol)
        {
            mdRow = ldRow;
            mdCol = ldCol;
        }
        /// <summary>
        /// Will check if the bullet has crossed,
        /// the upper limit of the passed WorldSquare.
        /// </summary>
        /// <param name="loWallSquare">
        /// Passed Empty WorldSquare.
        /// </param>
        public void HitWallUp(WorldSquare loEmptySquare)
        {
            if (moPosition.Y < loEmptySquare.Position.Y)
            {
                this.Die();
            }
        }
        /// <summary>
        /// Will check if the bullet has crossed,
        /// the lower limit of the passed WorldSquare.
        /// </summary>
        /// <param name="loWallSquare">
        /// Passed Empty WorldSquare.
        /// </param>
        public void HitWallDown(WorldSquare loEmptySquare)
        {
            if (moPosition.Y > loEmptySquare.Position.Y + loEmptySquare.Size)
            {
                this.Die();
            }
        }
        /// <summary>
        /// Will check if the bullet has crossed,
        /// the left side limit of the passed WorldSquare.
        /// </summary>
        /// <param name="loWallSquare">
        /// Passed Empty WorldSquare.
        /// </param>
        public void HitWallLeft(WorldSquare loEmptySquare)
        {
            if (moPosition.X < loEmptySquare.Position.X)
            {
                this.Die();
            }
        }
        /// <summary>
        /// Will check if the bullet has crossed,
        /// the right side limit of the passed WorldSquare.
        /// </summary>
        /// <param name="loWallSquare">
        /// Passed Empty WorldSquare.
        /// </param>
        public void HitWallRight(WorldSquare loEmptySquare)
        {
            if (moPosition.X > loEmptySquare.Position.X + loEmptySquare.Size)
            {
                this.Die();
            }
        }
        /// <summary>
        /// Will toggle the Bullet between being paused or not.
        /// </summary>
        public void Pause()
        {
            if (meBulletState != BulletState.Pause)
            {
                meBulletPreviousState = meBulletState;
                meBulletState = BulletState.Pause;
            }
            else if (meBulletState == BulletState.Pause)
            {
                meBulletState = meBulletPreviousState;
            }
        }

        //
        /* 
         * PRIVATE METHODS
         */
        //

        /// <summary>
        /// Will update all loLocation variables based off the moPosition vector
        /// </summary>
        private void LocationUpdate()
        {
            moBoundaryBox.X = (int)moPosition.X;
            moBoundaryBox.Y = (int)moPosition.Y;

        }
        /// <summary>
        /// Updates mdAlive bool for,
        /// use in class property
        /// </summary>
        private void AliveTracker()
        {
            if (meBulletState != BulletState.Dead)
            {
                mdAlive = true;
            }
            else
            {
                mdAlive = false;
            }

        }
        /// <summary>
        /// Will update the spawn area of the bullet
        /// </summary>
        private void SpawnPoint(Vector2 loLocation)
        {
            moPosition = loLocation;
            moBoundaryBox = new Rectangle((int)moPosition.X, (int)moPosition.Y, ConsWidth, ConsHeight);
            moVelocity = new Vector2();

        }
        /// <summary>
        /// Will handle bullet movement based on the direction its facing
        /// </summary>
        private void Move()
        {
            MoveForwards();
            moPosition += moVelocity;

        }
        /// <summary>
        /// Will move forwards, the bullet based on the direction its faced
        /// by applying the rotation angle to its moVelocity vector
        /// </summary>
        private void MoveForwards()
        {
            moVelocity = new Vector2(0, mdSpeed);
            moVelocity = RotateVector(moVelocity, mdRotationAngle);

        }
        /// <summary>
        /// The vector to rotate will be rotated,
        /// by angle in degrees,
        /// around it own vector,
        /// by using the following formula:
        /// X =
        /// -(cos(radians) * moVelocity.X - sin(radians) * moVelocity.Y)
        /// Y = 
        /// (sin(radians) * moVelocity.X - cos(radians) * moVelocity.Y)
        /// </summary>
        /// <param name="loVectorToRotate"></param>
        /// <param name="ldAngleInDegrees"></param>
        /// <returns></returns>
        private Vector2 RotateVector(Vector2 loVectorToRotate, float ldAngleInDegrees)
        {
            double angleInRadians = AngleToRadians(ldAngleInDegrees);
            double cosTheta = Math.Cos(angleInRadians);
            double sinTheta = Math.Sin(angleInRadians);

            return new Vector2(
                (float)
                -(cosTheta * loVectorToRotate.X - sinTheta * loVectorToRotate.Y),
                (float)
                (sinTheta * loVectorToRotate.X - cosTheta * loVectorToRotate.Y)
                );

        }
        /// <summary>
        /// Will convert degrees into radians
        /// </summary>
        /// <param name="ldInDegrees"> angle in degrees</param>
        private float AngleToRadians(float ldInDegrees)
        {
            return (float)(ldInDegrees * (Math.PI / 180));

        }

    }
} // End of namespace
