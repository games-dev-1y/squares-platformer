﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

/* Class Description:
 *  -   Particle Effects class.
 */

namespace JointProject2
{
    class Particle
    {

        //
        /*
         * VARIABLES & PROPERTIES
         */
        //

        private Vector2 moPosition;
        /// <summary>
        /// Gets the Particle's current position.
        /// </summary>
        public Vector2 Position
        { get { return moPosition; } }
        private Vector2 moVelocity;
        private int mdTimeAlive;
        private int mdTimeToLive;
        public enum ParticleState
        {
            Alive, Dead, Pause
        }
        private ParticleState meParticleState;
        private ParticleState meParticlePreviousState;
        /// <summary>
        /// Gets the particle's current state.
        /// </summary>
        public ParticleState State
        { get { return meParticleState; } }
        private bool mdAlive;
        /// <summary>
        /// Gets the particle's alive boolean
        /// </summary>
        public bool Alive
        { get { return mdAlive; } }

        //
        /* 
         * CONSTANT DECLARATION
         */
        //

        private const int TimeToLive = 60;
        private const float ParticleFriction = 0.9f;

        //
        /* 
         * CONSTRUCTOR
         */
        //

        /// <summary>
        /// Default Constructor:
        /// Intializes all our instance variables
        /// </summary>
        public Particle(Random loRandomNoGenerator)
        {
            mdTimeToLive = (int)(TimeToLive * (0.2f + 0.8f * loRandomNoGenerator.NextDouble()));
            mdTimeAlive = 0;
            mdAlive = false;
            meParticleState = ParticleState.Dead;
        }

        //
        /* 
         * PUBLIC METHODS
         */
        //

        /// <summary>
        /// Handles all particle update logic
        /// </summary>
        public void Update()
        {
            AliveTracker();
            switch (meParticleState)
            {
                case ParticleState.Pause:
                    break;
                case ParticleState.Dead:
                    Reset();
                    break;
                case ParticleState.Alive:
                    if (mdTimeAlive > mdTimeToLive)
                    {
                        meParticleState = ParticleState.Dead;
                    }
                    else
                    {
                        mdTimeAlive++;
                        Move();
                    }
                    break;
                default:
                    break;
            }
        }
        /// <summary>
        /// Overloaded Method:
        /// Will set the particle's velocity to the Passed Vector2
        /// and set it to alive.
        /// </summary>
        /// <param name="loVelocity">
        /// Passed Vector2, that will define what velocity the particle goes at.
        /// </param>
        public void SetVelocity(Vector2 loVelocity)
        {
            moVelocity = loVelocity;
            mdAlive = true;
        }
        /// <summary>
        /// Overloaded Method:
        /// Will set the particle's velocity to the Passed Vector2,
        /// with the Passed Vector2 being rotated by the Passed float,
        /// and set it to alive.
        /// </summary>
        /// <param name="loVelocity">
        /// Passed Vector2, that will define what velocity the particle goes at.
        /// </param>
        /// <param name="ldAngle">
        /// Passed float, that will define what angle of rotation the particle's velocity will be rotated by.
        /// </param>
        public void SetVelocity(Vector2 loVelocity, float ldAngle)
        {
            double ldAngleInRadians = (ldAngle * (Math.PI / 180));
            double ldCosTheta = Math.Cos(ldAngleInRadians);
            double ldSinTheta = Math.Sin(ldAngleInRadians);

            moVelocity = new Vector2(
                (float)
                (ldCosTheta * loVelocity.X - ldSinTheta * loVelocity.Y),
                (float)
                (ldSinTheta * loVelocity.X + ldCosTheta * loVelocity.Y)
                );
            mdAlive = true;
        }
        /// <summary>
        /// Will set the particle's position to the Passed Vector2,
        /// and set the particle's velocity to the Passed Vector2,
        /// with the Passed Vector2 being rotated by the Passed float,
        /// and set it to alive.
        /// </summary>
        /// <param name="loPosition">
        /// Passed Vector2, that will define what position the particle starts at.
        /// </param>
        /// <param name="loVelocity">
        /// Passed Vector2, that will define what velocity the particle goes at.
        /// </param>
        /// <param name="ldAngle">
        /// Passed float, that will define what angle of rotation the particle's velocity will be rotated by.
        /// </param>
        public void Activate(Vector2 loPosition, Vector2 loVelocity, float ldAngle)
        {
            double ldAngleInRadians = (ldAngle * (Math.PI / 180));
            double ldCosTheta = Math.Cos(ldAngleInRadians);
            double ldSinTheta = Math.Sin(ldAngleInRadians);

            moVelocity = new Vector2(
                (float)
                (ldCosTheta * loVelocity.X - ldSinTheta * loVelocity.Y),
                (float)
                (ldSinTheta * loVelocity.X + ldCosTheta * loVelocity.Y)
                );
            meParticleState = ParticleState.Alive;
            moPosition = loPosition;
        }
        /// <summary>
        /// Will reset the particle
        /// </summary>
        public void Reset()
        {
            Vector2 loOrigin = Vector2.Zero;
            moPosition = loOrigin;
            moVelocity = loOrigin;
            mdTimeAlive = 0;
            mdAlive = false;
        }
        /// <summary>
        /// Will toggle the Particle between being paused or not.
        /// </summary>
        public void Pause()
        {
            if (meParticleState != ParticleState.Pause)
            {
                meParticlePreviousState = meParticleState;
                meParticleState = ParticleState.Pause;
            }
            else
            {
                meParticleState = meParticlePreviousState;
            }
        }

        //
        /*
         * PRIVATE METHODS
         */
        //

        /// <summary>
        /// Will update the particle's position,
        /// by adding its velocity onto it's position.
        /// </summary>
        private void Move()
        {
            moPosition += moVelocity;
            moVelocity *= ParticleFriction;
        }
        /// <summary>
        /// Will update the alive boolean based on the particle's state.
        /// </summary>
        private void AliveTracker()
        {
            if (meParticleState != ParticleState.Dead)
            {
                mdAlive = true;
            }
            else
            {
                mdAlive = false;
            }
        }

    }
} // End of namespace