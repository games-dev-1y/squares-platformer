﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

/* Class Description:
 *  Will represent a square in the game world
 */

namespace JointProject2
{
    class WorldSquare
    {
        //
        /*
         * VARIABLES & PROPERTIES
         */
        //

        // Position of the square (used for the Draw() method)
        private Vector2 moPosition;
        /// <summary>
        /// Gets the Position Vector of the Square
        /// </summary>
        public Vector2 Position
        { get { return moPosition; } }
        // Position of the square in the array
        private int mdRow;
        private int mdCol; // Col = Collumn
        // Used to determine the square type
        public enum SquareType
        {
            Empty, Wall, Exit
        }
        SquareType meSquareType;
        /// <summary>
        /// Gets the Type of SquareType
        /// </summary>
        public SquareType Type
        { get { return meSquareType; } }

        //
        /*
         * CONSTANT DECLARATION
         */
        //

        // Square Size, namely its Width/Height (they are the same)
        const int SquareSize = 20;
        /// <summary>
        /// Gets the Size of the square
        /// </summary>
        public int Size
        { get { return SquareSize; } }

        //
        /*
         * CONSTRUCTORS
         */
        //

        /// <summary>
        /// Default Constructor,
        /// Creates a empty square at (0,0)
        /// </summary>
        public WorldSquare()
        {
            moPosition = new Vector2(0f);
            mdRow = 0;
            mdCol = 0;
            meSquareType = SquareType.Empty;
        }
        /// <summary>
        /// Constructor,
        /// Creates a empty square at the passed row and collumn
        /// </summary>
        /// <param name="ldRow">Passed Row</param>
        /// <param name="ldCol">Passed Collumn</param>
        public WorldSquare(int ldRow, int ldCol)
        {
            moPosition = new Vector2(SquareSize * ldCol, SquareSize * ldRow);
            mdRow = ldRow;
            mdCol = ldCol;
            meSquareType = SquareType.Empty;
        }
        /// <summary>
        /// Constructor,
        /// Creates a square using the passed row and collumn,
        /// using the SquareType to determine what type of square it is
        /// </summary>
        /// <param name="ldRow">
        /// Passed row, determines y-moPosition
        /// </param>
        /// <param name="ldCol">
        /// Passed collumn, determines x-moPosition
        /// </param>
        /// <param name="leType">
        /// Passed SquareType, determines what type of square is generated
        /// </param>
        public WorldSquare(int ldRow, int ldCol, SquareType leType)
        {
            moPosition = new Vector2(SquareSize * ldCol, SquareSize * ldRow);
            mdRow = ldRow;
            mdCol = ldCol;
            meSquareType = leType;
        }

        public static int GetSize()
        {
            return SquareSize;
        }

    }
} // End of namespace