﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

/* Class Description
 * 
 */

namespace JointProject2
{
    class EnemySquareBasic : JointProject2.Enemy
    {
        //
        /* 
         * VARIABLES & PROPERTIES
         */
        //

        /// <summary>
        /// Gets the current ConsWidth of the EnemyBasic.
        /// </summary>
        public override int Width
        { get { return base.Width; } }
        /// <summary>
        /// Gets the current ConsHeight of the EnemyBasic.
        /// </summary>
        public override int Height
        { get { return base.Height; } }

        //
        /* 
         * CONSTANT DECLARATION
         */
        //

        private const float MoveSpeed = 1f;
        private readonly Vector2 GroundSpeed = new Vector2(MoveSpeed, 0f);

        //
        /* 
         * CONSTRUCTORS
         */
        //

        /// <summary>
        /// Default Constructor:
        /// Will create a basic square enemy at (0,0)
        /// </summary>
        public EnemySquareBasic()
        {
            moPosition = new Vector2(0f);
            moVelocity = new Vector2(0f);
            meEnemyState = EnemyState.Ground;
        }

        //
        /* 
         * OVERRIDE METHODS
         */
        //

        /// <summary>
        /// Will handle all basic enemy square logic
        /// </summary>
        public override void Update()
        {
            switch (meEnemyState)
            {
                case EnemyState.Pause:
                case EnemyState.Dead:
                    break;
                case EnemyState.Ground:
                    SetMovement();
                    goto case EnemyState.Alive;
                case EnemyState.Attack:
                case EnemyState.SeePlayer:
                case EnemyState.Patrol:
                case EnemyState.Alive:
                    Move();
                    break;
                default:
                    break;
            }
        }
        /// <summary>
        /// Will handle EnemySquareBasic movement,
        /// by calling the base class's Move() method. 
        /// </summary>
        protected override void Move()
        {
            base.Move();
        }

        //
        /* 
         * PUBLIC METHODS
         */
        //

        /// <summary>
        /// When enemy hits the left side of a wall,
        /// will invert the BasicSquare moVelocity.
        /// </summary>
        /// <param name="loWallSquare">
        /// Passed Empty Square of WorldSquare type,
        /// that the enemy will anchor to.
        /// </param>
        public void HitLeftWall(WorldSquare loEmptySquare)
        {
            if (moPosition.X - GroundSpeed.X < loEmptySquare.Position.X)
            {
                moVelocity.X = GroundSpeed.X;
            }
        }
        /// <summary>
        /// When enemy hits the right side of a wall,
        /// will invert the BasicSquare moVelocity.
        /// </summary>
        /// <param name="loWallSquare">
        /// Passed Empty Square of WorldSquare type,
        /// that the enemy will anchor to.
        /// </param>
        public void HitRightWall(WorldSquare loEmptySquare)
        {
            if (moPosition.X > loEmptySquare.Position.X + loEmptySquare.Size - this.Width)
            {
                moVelocity.X = -GroundSpeed.X;
            }
        }
        /// <summary>
        /// Runs a loop backtracing the enemie's steps,
        /// until they are outside the current square.
        /// </summary>
        /// <param name="loWallSquare">
        /// Passed Wall Square of WorldSquare type,
        /// that the enemy will try to exit.
        /// </param>
        public void InsideWall(WorldSquare loWallSquare)
        {
            moVelocity = -moVelocity;
            while (
                moPosition.X <= loWallSquare.Position.X &&
                moPosition.X >= loWallSquare.Position.X + loWallSquare.Size - this.Width)
            {
                moPosition.X += moVelocity.X;
            }
            while (
                moPosition.Y <= loWallSquare.Position.Y &&
                moPosition.Y >= loWallSquare.Position.Y + loWallSquare.Size - this.Height)
            {
                moPosition.Y += moVelocity.Y;
            }
        }

        //
        /* 
         * PRIVATE METHODS
         */
        //

        /// <summary>
        /// Will set the BasicSquare's moVelocity,
        /// to ground mdSpeed if not previously set
        /// </summary>
        private void SetMovement()
        {
            if (!(moVelocity.X == GroundSpeed.X || moVelocity.X == -GroundSpeed.X))
            {
                moVelocity = GroundSpeed;
            }
        }

    }
} // End of namespace