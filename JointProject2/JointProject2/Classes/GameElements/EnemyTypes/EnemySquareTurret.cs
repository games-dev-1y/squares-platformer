﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

/* Class Description:
 *  This class will inhenrit the base Enemy class,
 *  and will contain the functions of a turret.
 */

namespace JointProject2
{
    class EnemySquareTurret : JointProject2.Enemy
    {
        //
        /* 
         * VARIABLES & PROPERTIES
         */
        //

        /// <summary>
        /// Get's the Enemy turret's eye moPosition
        /// </summary>
        public override Vector2 EyePosition
        { get { return base.EyePosition; } }
        /// <summary>
        /// Gets the the current ConWidth of the EnemyTurret.
        /// </summary>
        public override int Width
        { get { return ConWidth; } }
        /// <summary>
        /// Gets the current ConHeight of the EnemyTurret.
        /// </summary>
        public override int Height
        { get { return ConHeight; } }
        private Bullet[] maBullets;
        /// <summary>
        /// Gets the Bullet Array.
        /// </summary>
        public Bullet[] Bullets
        { get { return maBullets; } }
        private Vector2 moFiringPosition;
        private int mdFireRate;
        private float mdFiringDirection;
        private int mdBulletsFired;
        /// <summary>
        /// Gets the maximum number of bullets fireable by any turret.
        /// </summary>
        public static int MaximumBullets
        { get { return MaxBullets; } }

        //
        /* 
         * CONSTANT DECLARATION
         */
        //

        private const int ConHeight = 20;
        private const int ConWidth = 20;
        private const int BulletsFired = 4;
        private const int MaxBullets = 80;
        private const int WhenToFire = 30000;
        private const int FiringRate = 1;
        private const int FiringAngle = 90;

        //
        /* 
         * CONSTRUCTORS
         */
        //

        /// <summary>
        /// Default Constructor:
        /// Will create a basic square enemy at (0,0)
        /// </summary>
        public EnemySquareTurret()
        {
            moPosition = new Vector2(0f);
            moVelocity = new Vector2(0f);
            meEnemyState = EnemyState.Alive;
            maBullets = new Bullet[MaxBullets];
            for (int i = 0; i < maBullets.Length; i++)
            {
                maBullets[i] = new Bullet();
            }
            mdBulletsFired = 0;
            mdFireRate = WhenToFire - (FiringRate * 2);
        }

        //
        /* 
         * PUBLIC METHODS
         */
        //

        /// <summary>
        /// Will handle all basic enemy square logic
        /// </summary>
        public override void Update()
        {
            switch (meEnemyState)
            {
                case EnemyState.Pause:
                case EnemyState.Dead:
                    break;
                case EnemyState.SeePlayer:
                case EnemyState.Patrol:
                case EnemyState.Attack:
                case EnemyState.Air:
                case EnemyState.Ground:
                case EnemyState.Alive:
                default:
                    for (int ldBulletIndex = 0; ldBulletIndex < maBullets.Length; ldBulletIndex++)
                    {
                        if (mdFireRate > WhenToFire)
                        {
                            for (int i = mdBulletsFired; i < maBullets.Length && i < BulletsFired + mdBulletsFired; i++)
                            {
                                maBullets[i].SetRowCollumn(this.Row, this.Col);
                                maBullets[i].Fire(mdFiringDirection);
                                mdFiringDirection += FiringAngle;
                            }
                            mdBulletsFired += BulletsFired;
                            mdFiringDirection = 0;
                            mdFireRate = 0;
                            if (mdBulletsFired >= MaxBullets)
                            {
                                mdBulletsFired = 0;
                            }
                            break;
                        }
                        else
                        {
                            mdFireRate += FiringRate;
                        }
                        maBullets[ldBulletIndex].Update(moFiringPosition);
                    }

                    break;
            }
        }
        /// <summary>
        /// Sets the current moPosition vector to the 2 passed floats,
        /// and sets the firing position to be centered.
        /// </summary>
        /// <param name="mdX">
        /// Passed float, used to set the X coordinate.
        /// </param>
        /// <param name="mdY">
        /// Passed float, used to set the Y coordinate.
        /// </param>
        public override void SetPosition(float mdX, float mdY)
        {
            base.SetPosition(mdX, mdY);
            moFiringPosition = new Vector2(moPosition.X + (ConWidth / 2), moPosition.Y + (ConHeight / 2));
        }
        /// <summary>
        /// Will toggle the Enemy between being paused or not,
        /// and will pause all of the EnemySquareTurret's Bullet's.
        /// </summary>
        public override void Pause()
        {
            for (int ldIndex = 0; ldIndex < maBullets.Length; ldIndex++)
            {
                maBullets[ldIndex].Pause();
            }
            base.Pause();
        }

        //
        /* 
         * PRIVATE METHODS
         */
        //

        

    }
} // End of namespace