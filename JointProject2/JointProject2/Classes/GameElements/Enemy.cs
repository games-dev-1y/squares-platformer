﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

/* Class Description:
 *  This is the Enemy base class from which all enemies are based off.
 */

namespace JointProject2
{
    class Enemy
    {
        //
        /* 
         * VARIABLES & PROPERTIES
         */
        //
        
        // Enemy's positions
        protected Vector2 moPosition;
        /// <summary>
        /// Gets the enemy's current moPosition
        /// </summary>
        public virtual Vector2 Position
        { get { return moPosition; } }
        protected Vector2 moVelocity;
        protected Vector2 moEyePosition;
        // Enemy's Row and Collumn
        protected int mdRow, mdCol;
        /// <summary>
        /// Gets or Sets the enemy's current row.
        /// </summary>
        public virtual int Row
        {
            get { return mdRow; }
            set { mdRow = value; }
        }
        /// <summary>
        /// Gets or Sets the enemy's current collumn.
        /// </summary>
        public virtual int Col
        {
            get { return mdCol; }
            set { mdCol = value; }
        }
        protected int mdPreviousRow, mdPreviousCol;
        /// <summary>
        /// Gets or Sets the enemy's previous row moPosition.
        /// </summary>
        public virtual int PreviousRow
        {
            get { return mdPreviousRow; }
            set { mdPreviousRow = value; }
        }
        /// <summary>
        /// Gets or Sets the enemy's previous collumn moPosition.
        /// </summary>
        public virtual int PreviousCol
        {
            get { return mdPreviousCol; }
            set { mdPreviousCol = value; }
        }
        // Enemy eye moPosition
        /// <summary>
        /// Gets the enemy eye's current moPosition
        /// </summary>
        public virtual Vector2 EyePosition
        { get { return moEyePosition; } }
        // Enemy's Health
        protected  int mdHealth;
        /// <summary>
        /// Gets the enemy's current health
        /// </summary>
        public virtual int Health
        { get { return mdHealth; } }
        // Enemy's State
        public enum EnemyState
        {
            Alive, Dead, SeePlayer, Patrol, Attack, Air, Ground, Pause
        }
        protected EnemyState meEnemyState;
        private EnemyState meEnemyPreviousState;
        /// <summary>
        /// Gets the enemy's current state
        /// </summary>
        public EnemyState State
        { get { return meEnemyState; } }

        //
        /* 
         * CONSTANT DECLARATION
         */
        //

        // Movement related constants
        protected readonly Vector2 Zero = Vector2.Zero;
        // Gravity that affects land based enemies
        protected readonly Vector2 Gravity = new Vector2(0f, 0.1f);
        // Default Dimensions
        protected const int ConsWidth = 15;
        /// <summary>
        /// Gets the ConsWidth of the Enemy
        /// </summary>
        public virtual int Width
        { get { return ConsWidth; } }
        protected const int ConsHeight = 15;
        /// <summary>
        /// Gets the ConsHeight of the Enemy
        /// </summary>
        public virtual int Height
        { get { return ConsHeight; } }

        //
        /* 
         * CONSTRUCTORS
         */
        //

        public Enemy()
        {
            moPosition = new Vector2(0f);
            moVelocity = new Vector2(0f);
            moEyePosition = new Vector2(0f);
            mdHealth = 1;
            meEnemyState = EnemyState.Alive;
        }

        //
        /* 
         * PUBLIC METHODS
         */
        //

        /// <summary>
        /// Handles all enemy logic
        /// </summary>
        public virtual void Update()
        { }

        /// <summary>
        /// Sets the current moPosition vector to the 2 passed floats.
        /// </summary>
        /// <param name="mdX">
        /// Passed float, used to set the X coordinate.
        /// </param>
        /// <param name="mdY">
        /// Passed float, used to set the Y coordinate.
        /// </param>
        public virtual void SetPosition(float mdX, float mdY)
        {
            moPosition = new Vector2(mdX, mdY);
        }
        /// <summary>
        /// Sets the current row and collumn of the enemy
        /// </summary>
        /// <param name="ldRow">
        /// Passed integer, used to set the row moPosition.
        /// </param>
        /// <param name="ldCol">
        /// Passed integer, used to set the collumn moPosition.
        /// </param>
        public virtual void SetRowCollumn(int ldRow, int ldCol)
        {
            mdPreviousCol = mdCol;
            mdPreviousRow = mdRow;
            mdCol = ldCol;
            mdRow = ldRow;
        }
        /// <summary>
        /// Will set the enemySquare to a ground state
        /// </summary>
        /// <param name="loWallSquare">
        /// Passed WorldSquare, this is the empty
        /// </param>
        public virtual void Landed(WorldSquare loEmptySquare)
        { }
        /// <summary>
        /// Will toggle the Enemy between being paused or not.
        /// </summary>
        public virtual void Pause()
        {
            if (meEnemyState != EnemyState.Pause)
            {
                meEnemyPreviousState = meEnemyState;
                meEnemyState = EnemyState.Pause;
            }
            else
            {
                meEnemyState = meEnemyPreviousState;
            }
        }

        //
        /* 
         * PRIVATE METHODS
         */
        //

        /// <summary>
        /// Will handle enemy movement,
        /// incrementing the Position Vector by the Velocity Vector.
        /// </summary>
        protected virtual void Move()
        {
            moPosition += moVelocity;
        }

    }
} // End of namespace