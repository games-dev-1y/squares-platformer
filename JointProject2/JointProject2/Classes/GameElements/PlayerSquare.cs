﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

/* Class Description:
 *  Will pertain to our player controlled square.
 */

namespace JointProject2
{
    class PlayerSquare
    {
        //
        /*
         * VARIABLES & PROPERTIES
         */
        //

        // Position and related vectors
        private Vector2 moPosition; // Player moPosition
        /// <summary>
        /// Gets the Players Position
        /// </summary>
        public Vector2 Position
        { get { return moPosition; } }
        /// <summary>
        /// Gets the Player's bottom right position.
        /// </summary>
        public Vector2 BottomRight
        { get { return new Vector2(moPosition.X + ConsWidth, moPosition.Y + ConsHeight); } }
        /// <summary>
        /// Gets the Player's bottom left position.
        /// </summary>
        public Vector2 BottomLeft
        { get { return new Vector2(moPosition.X, moPosition.Y + ConsHeight); } }
        /// <summary>
        /// Gets the Player's center position.
        /// </summary>
        public Vector2 Center
        { get { return new Vector2(moPosition.X + (ConsWidth / 2f), moPosition.Y + (ConsHeight / 2f)); } }
        private Vector2 moVelocity; // Player Velocity
        private Vector2 moEyePosition;
        /// <summary>
        /// Gets the Eye's Position
        /// </summary>
        public Vector2 EyePosition
        {
            get { return moEyePosition; }
            set { moEyePosition = value; }
        }
        private int mdRow, mdCol;   //Row and Collumn
        /// <summary>
        /// Gets the current Row the player is in
        /// </summary>
        public int Row
        {
            get { return mdRow; }
            set { mdRow = value; }
        }
        /// <summary>
        /// Gets the current Collumn the player is in
        /// </summary>
        public int Col
        {
            get { return mdCol; }
            set { mdCol = value; }
        }
        private int mdPreviousRow, mdPreviousCol; // Previous Row and Collumn, use in case of emergencies
        /// <summary>
        /// Gets or Sets the Row the player was previously in.
        /// </summary>
        public int PreviousRow
        {
            get { return mdPreviousRow; }
            set { mdPreviousRow = value; }
        }
        /// <summary>
        /// Gets or Sets the Collumn the player was previously in.
        /// </summary>
        public int PreviousCol
        {
            get { return mdPreviousCol; }
            set { mdPreviousCol = value; }
        }
        // Input related
        private KeyboardState moKeyboard;
        private Keys[] maKeysPressed, maOldKeysPressed;
        // Player States
        public enum PlayerState
        {
            Dead, Alive, Paused, AirUp, AirDown, Ground
        }
        private PlayerState mePlayerState;
        private PlayerState mePlayerPreviousState;
        /// <summary>
        /// Gets the players current state
        /// </summary>
        public PlayerState State
        { get { return mePlayerState; } }
        /// <summary>
        /// Gets the players previous state.
        /// </summary>
        public PlayerState StateOld
        { get { return mePlayerPreviousState; } }
        // Player Movement Direction
        public enum Movement
        {
            Left, Right, AirLeft, AirRight, None
        }
        private Movement meMovement;
        /// <summary>
        /// Gets the players movement direction
        /// </summary>
        public Movement Direction
        { get { return meMovement; } }
        // Used to check if player has double jumped
        private bool mdDoubleJumped;
        // Player's Name
        private string mdName;
        /// <summary>
        /// Get's or Set's the player's name.
        /// </summary>
        public string Name
        {
            get { return mdName; }
            set { mdName = value; }
        }
        private bool mdAlive;
        /// <summary>
        /// Get's the player's alive.
        /// </summary>
        public bool Alive
        { get { return mdAlive; } }

        //
        /*
         * CONSTANT DECLARATION
         */
        //

        // Represents initial jump momentum
        private readonly Vector2 JumpMomentum = new Vector2(0f, -4f);
        // Represents mdSpeed at which player falls
        private readonly Vector2 Gravity = new Vector2(0f, 0.15f);
        // Represents the default player movement
        private readonly Vector2 MoveLeft = new Vector2(-2f, 0f);
        private readonly Vector2 MoveRight = new Vector2(2f, 0f);
        private readonly Vector2 AirRight = new Vector2(1f, 0f);
        private readonly Vector2 AirLeft = new Vector2(-1f, 0f);
        // Represents the various positions for the eye square
        private readonly Vector2 EyeCenter = new Vector2(3f, 3f);
        private readonly Vector2 EyeLeft = new Vector2(0f, 3f);
        private readonly Vector2 EyeRight = new Vector2(7f, 3f);
        private readonly Vector2 EyeAirLeft = new Vector2(1f, 1f);
        private readonly Vector2 EyeAirRight = new Vector2(7f, 1f);
        // Represents a 0 vector
        private readonly Vector2 Zero = Vector2.Zero;
        // Player dimensions (MUST CORRESPOND TO PLAYER SQUARE TEXTURE)
        private const int ConsWidth = 10;
        /// <summary>
        /// Returns the Constant Width of the player
        /// </summary>
        public int Width
        { get { return ConsWidth; } }
        private const int ConsHeight = 10;
        /// <summary>
        /// Returns the Constant Height of the player
        /// </summary>
        public int Height
        { get { return ConsHeight; } }


        //
        /*
         * CONSTRUCTORS
         */
        //

        /// <summary>
        /// Default Constructor,
        /// Creates player at origin (0,0)
        /// </summary>
        public PlayerSquare()
        {
            moPosition = new Vector2(0f);
            moVelocity = new Vector2(0f);
            mdCol = 0;
            mdPreviousCol = 0;
            mdRow = 0;
            mdPreviousRow = 0;
            mePlayerState = PlayerState.AirDown;
            mdAlive = true;
        }

        //
        /*
         * PUBLIC METHODS
         */
        //

        /// <summary>
        /// Handles player update logic
        /// </summary>
        public void Update()
        {
            switch (mePlayerState)
            {
                case PlayerState.Paused:
                    break;
                case PlayerState.Dead:
                    break;
                case PlayerState.AirUp:
                    if (moVelocity.Y > 0)
                    {
                        mePlayerState = PlayerState.AirDown;
                    }
                    goto case PlayerState.AirDown;
                case PlayerState.AirDown:
                    moVelocity.Y += Gravity.Y;
                    AirInput();
                    goto case PlayerState.Alive;
                case PlayerState.Ground:
                    GroundInput();
                    mdDoubleJumped = false;
                    goto case PlayerState.Alive;
                case PlayerState.Alive:
                    Move();
                    moPosition += moVelocity;
                    break;
                default:
                    break;
            }
        }
        /// <summary>
        /// Will spawn using the passed floats as a x/y coordinate,
        /// while also setting the player's velocity and movement to 0/none.
        /// </summary>
        /// <param name="ldX">
        /// Passed float will determine the X coordinate.
        /// </param>
        /// <param name="ldY">
        /// Passed float will determine the Y coordinate.
        /// </param>
        public void Spawn(float ldX, float ldY)
        {
            moPosition.X = (int)ldX;
            moPosition.Y = (int)ldY;
            moVelocity = Zero;
            meMovement = Movement.None;
        }
        /// <summary>
        /// Will set the players x and y moPosition to
        /// passed values
        /// </summary>
        /// <param name="ldX">Passed X value</param>
        /// <param name="ldY">Passed Y value</param>
        public void SetPosition(float ldX, float ldY)
        {
            moPosition = new Vector2(ldX, ldY);
        }
        /// <summary>
        /// Will set the players x and y Position to the passed Vector2
        /// </summary>
        /// <param name="ldLocation">
        /// Passed Vector2 that will define what location the player is relocated to.
        /// </param>
        public void SetPosition(Vector2 ldLocation)
        {
            moPosition = ldLocation;
        }
        /// <summary>
        /// Will set the players row value to
        /// the passed value
        /// </summary>
        /// <param name="ldRow">Passed Row value</param>
        public void SetRow(int ldRow)
        {
            mdRow = ldRow;
        }
        /// <summary>
        /// Will set the players collumn value to
        /// the passed value
        /// </summary>
        /// <param name="ldCol">Passed Collumn value</param>
        public void SetCol(int ldCol)
        {
            mdCol = ldCol;
        }
        /// <summary>
        /// Will revive the player.
        /// </summary>
        public void SetAlive()
        {
            mePlayerState = PlayerState.AirDown;
        }
        /// <summary>
        /// Will anchor player and set him to ground state
        /// </summary>
        /// <param name="loWallSquare">
        /// Passed Empty Square of WorldSquare type,
        /// that the player will anchor to.
        /// </param>
        public void Landed(WorldSquare loEmptySquare)
        {
            if (mePlayerState == PlayerState.AirDown)
            {
                moPosition.Y = loEmptySquare.Position.Y + loEmptySquare.Size - this.Height;
                moVelocity.Y = 0f;
                mePlayerState = PlayerState.Ground;
            }
        }
        /// <summary>
        /// Will set player at passed WorldSquare Height
        /// </summary>
        /// <param name="loWallSquare">
        /// Passed Empty Square of WorldSquare type,
        /// that the player will anchor to.
        /// </param>
        public void HitRoof(WorldSquare loWallSquare)
        {
            if (mePlayerState == PlayerState.AirUp)
            {
                if (moPosition.Y + moVelocity.Y <= loWallSquare.Position.Y + loWallSquare.Size)
                {
                    moVelocity.Y = Zero.Y;
                    mePlayerState = PlayerState.AirDown;
                }
            }
        }
        /// <summary>
        /// There is no ground under the player so,
        /// set him to falling down state.
        /// </summary>
        public void NoGround()
        {
            if (mePlayerState == PlayerState.Ground)
            {
                mePlayerState = PlayerState.AirDown;
            }
        }
        /// <summary>
        /// Will stop player from traversing left past the passed WorldSquare
        /// </summary>
        /// <param name="loWallSquare">
        /// Passed Empty Square, of which the player cannot exit the left side.
        /// </param>
        public void HitLeftWall(WorldSquare loEmptySquare)
        {
            if (moPosition.X < loEmptySquare.Position.X)
            {
                switch (mePlayerState)
                {
                    case PlayerState.AirUp:
                    case PlayerState.AirDown:
                    case PlayerState.Ground:
                        moVelocity.X = Zero.X;
                        moPosition.X = loEmptySquare.Position.X;
                        break;
                    case PlayerState.Dead:
                    case PlayerState.Paused:
                    case PlayerState.Alive:
                    default:
                        break;
                }
            }
        }
        /// <summary>
        /// Will stop player from traversing right past the passed WorldSquare
        /// </summary>
        /// <param name="loWallSquare">
        /// Passed Empty Square, of which the player cannot exit the right side.
        /// </param>
        public void HitRightWall(WorldSquare loEmptySquare)
        {
            if (moPosition.X > loEmptySquare.Position.X + loEmptySquare.Size - this.Width)
            {
                switch (mePlayerState)
                {
                    case PlayerState.AirUp:
                    case PlayerState.AirDown:
                    case PlayerState.Ground:
                        moVelocity.X = Zero.X;
                        moPosition.X = loEmptySquare.Position.X + loEmptySquare.Size - this.Width;
                        break;
                    case PlayerState.Dead:
                    case PlayerState.Paused:
                    case PlayerState.Alive:
                    default:
                        break;
                }
            }
        }
        /// <summary>
        /// Will stop player from traversing into the Passed WorldSquare.
        /// </summary>
        /// <param name="loWallSquare">
        /// Passed WorldSquare Wall Square, of which the player cannot enter.
        /// </param>
        public void HitCornerWall(WorldSquare loWallSquare)
        {
            switch (mePlayerState)
            {
                case PlayerState.AirUp:
                case PlayerState.AirDown:
                    if (moPosition.X + moVelocity.X > loWallSquare.Position.X)
                    {
                        moPosition.X -= moVelocity.X;
                    }
                    else if (moPosition.X + moVelocity.X < loWallSquare.Position.X + loWallSquare.Size)
                    {
                        moPosition.X -= moVelocity.X;
                    }
                    break;
                case PlayerState.Ground:
                case PlayerState.Paused:
                case PlayerState.Dead:
                case PlayerState.Alive:
                default:
                    break;
            }
        }
        /// <summary>
        /// Will undo the steps of the player until,
        /// player is no longer outside of the previous square
        /// </summary>
        public void EjectOutOfWall(WorldSquare loPreviousSquare)
        {
            if (moPosition.X < loPreviousSquare.Position.X)
            {
                moPosition.X = loPreviousSquare.Position.X;
                mdCol = mdPreviousCol;
            }
            else if (moPosition.X > loPreviousSquare.Position.X + loPreviousSquare.Size + this.Width)
            {
                moPosition.X = loPreviousSquare.Position.X + loPreviousSquare.Size + this.Width;
                mdCol = mdPreviousCol;
            }
            if (moPosition.Y < loPreviousSquare.Position.Y)
            {
                moPosition.Y = loPreviousSquare.Position.Y;
                mdRow = mdPreviousRow;
            }
            else if (moPosition.Y > loPreviousSquare.Position.Y + loPreviousSquare.Size + this.Height)
            {
                moPosition.Y = loPreviousSquare.Position.Y + loPreviousSquare.Size + this.Height;
                mdRow = mdPreviousRow;
            }
        }
        /// <summary>
        /// Will kill the player
        /// </summary>
        public void Die()
        {
            mePlayerState = PlayerState.Dead;
        }
        /// <summary>
        /// Will toggle the PlayerSquare between being paused or not.
        /// </summary>
        public void Pause()
        {
            if (mePlayerState != PlayerState.Paused)
            {
                mePlayerPreviousState = mePlayerState;
                mePlayerState = PlayerState.Paused;
            }
            else if (mePlayerState == PlayerState.Paused)
            {
                mePlayerState = mePlayerPreviousState;
            }
        }

        //
        /*
         * PRIVATE METHODS
         */
        //

        /// <summary>
        /// Called every update to verify any player input
        /// Will allow player to be controlled based on input,
        /// only while player is on the ground
        /// </summary>
        private void GroundInput()
        {
            RegisterInput();

            if (maKeysPressed.Length > 0) // This checks if something was pressed else it immediatly skips over
            {
                for (int i = 0; i < maKeysPressed.Length; i++)
                {
                    if (maKeysPressed[i] == Keys.Left)
                    {
                        meMovement = Movement.Left;
                    }
                    else if (maKeysPressed[i] == Keys.Right)
                    {
                        meMovement = Movement.Right;
                    }
                    else
                    {
                        meMovement = Movement.None;
                    }
                    if (maKeysPressed[i] == Keys.Space)
                    {
                        Jump(JumpMomentum);
                    }
                }
            }
            else
            {
                meMovement = Movement.None;
            }
        }
        /// <summary>
        /// Called every update to verify any player input
        /// Will allow player to be controlled based on input,
        /// only while player is in the air
        /// </summary>
        private void AirInput()
        {
            RegisterInput();

            if (maKeysPressed.Length > 0)
            {
                for (int i = 0; i < maKeysPressed.Length; i++)
                {
                    if (maKeysPressed[i] == Keys.Left)
                    {
                        if (meMovement != Movement.Left)
                        {
                            meMovement = Movement.AirLeft;
                        }
                    }
                    if (maKeysPressed[i] == Keys.Right)
                    {
                        if (meMovement != Movement.Right)
                        {
                            meMovement = Movement.AirRight;
                        }
                    }
                    if (maKeysPressed[i] == Keys.Space)
                    {
                        if (mePlayerState == PlayerState.AirDown && mdDoubleJumped == false)
                        {
                            if (maOldKeysPressed != null &&
                                maOldKeysPressed.Length > 0 &&
                            i < maOldKeysPressed.Length)
                            {
                                if (maOldKeysPressed[i] != Keys.Space)
                                {
                                    mdDoubleJumped = true;
                                    Jump(JumpMomentum);
                                }
                            }
                            else
                            {
                                mdDoubleJumped = true;
                                Jump(JumpMomentum);
                            }
                        }
                    }
                }
            }
        }
        /// <summary>
        /// Sets the Velocity vector based what movement state the player is in,
        /// also sets the eye's moPosition based on the movement state.
        /// </summary>
        private void Move()
        {
            switch (meMovement)
            {
                case Movement.Left:
                    moVelocity.X = MoveLeft.X;
                    moEyePosition = moPosition + EyeLeft;
                    break;
                case Movement.Right:
                    moVelocity.X = MoveRight.X;
                    moEyePosition = moPosition + EyeRight;
                    break;
                case Movement.AirLeft:
                    moVelocity.X = AirLeft.X;
                    moEyePosition = moPosition + EyeAirLeft;
                    break;
                case Movement.AirRight:
                    moVelocity.X = AirRight.X;
                    moEyePosition = moPosition + EyeAirRight;
                    break;
                case Movement.None:
                default:
                    moVelocity.X = Zero.X;
                    moEyePosition = moPosition + EyeCenter;
                    break;
            }
        }
        /// <summary>
        /// Sets the Velocity vectors Y value to the JumpMomentum's Y value,
        /// setting the player into the air state
        /// </summary>
        private void Jump(Vector2 loJumpVector)
        {
            moVelocity.Y = loJumpVector.Y;
            mePlayerState = PlayerState.AirUp;
        }
        /// <summary>
        /// Will update the KeyboardState,
        /// and register all keys pressed during current frame
        /// </summary>
        private void RegisterInput()
        {
            moKeyboard = Keyboard.GetState();
            maOldKeysPressed = maKeysPressed;
            maKeysPressed = moKeyboard.GetPressedKeys();
        }

    }
} // End of namespace