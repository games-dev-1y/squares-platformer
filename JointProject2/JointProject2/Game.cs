using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Xml;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

/* Name: Rafael Girao
 * Student ID: C00203250
 * Joint Project 2:
 *  - Make a Game in XNA using,
 *  2D arrays as the map layout.
 */
/* Prefix System:
 *  In this game I will be using a prefix system,
 *  for all our variables and classes.
 *  Details:
 *      -   m*  : is the starting letter for all,
 *      class instance variables.
 *      -   l*  : this starting letter is for all,
 *      local variables (their lifetime is limited to,
 *      a method or loop)
 *      -   *o* : this letter indicates that,
 *      the relating variable is of type Object
 *      -   *d* : this letter indicates that,
 *      the relating variable is in fact a data type variable,
 *      for example (integers, floats, bools, doubles)
 *      -   *e* : this letter is used only to indicate a enum
 *      -   *a* : this letter is used to indicate a array.
 *      -   *i* : this letter is used to indicate a list.
 */
/* Worked on:
 *  04th/03
 *      from:   09:00
 *      to:     11:00 (2 hours)
 *  11th/03
 *      from:   09:00
 *      to:     11:00 (2 hours)
 *  15th/03
 *      from:   13:00
 *      to:     15:00 (2 hours)
 *  21st/03 to 25th/03
 *      from:   13:00
 *      to:     19:00 (6 hours x 5 = 30 hours)
 *  28th/03 to 01st/04
 *      from:   13:00
 *      to:     19:00 (6 hours x 5 = 30 hours)
 *  05th/04
 *      from:   16:00
 *      to:     19:00 (3 hours)
 *  07th/04
 *      from:   10:00
 *      to:     20:00 (10 hours)
 *  11th/04
 *      from:   10:00
 *      to:     20:00 (10 hours)
 *  14th/04
 *      from:   13:00
 *      to:     20:00 (7 hours)
 *  15th/04
 *      from:   11:00
 *      to:     13:00 (2 hours)
 *  17th/04
 *      from:   20:00
 *      to:     24:00 (4 hours)
 * 
 * Total time spent:
 *      102h:00min
 */
/* Known bugs:
 *  -   None
 */


namespace JointProject2
{
    /// <summary>
    /// This is the main type for your game
    /// </summary>
    public class Game : Microsoft.Xna.Framework.Game
    {
        //
        ///
        //// VARIABLES & PROPERTIES
        ///
        //

        /* Graphics and drawing object declaration */
        private GraphicsDeviceManager moGraphics;
        private SpriteBatch spriteBatch;

        // A run once bool for each game state
        private bool mdRunOnce;
        private bool mdPaused;
        // Game window dimensions
        private int mdWindowWidth;
        private int mdWindowHeight;

        // GAME STATES
        public enum GameState
        {
            SplashScreen, MainMenu, Options, Loading, InGame, GameOver, Exit
        }
        private GameState meGameState;
        // Current Level
        private int mdLevel;
        // World Array Variable
        WorldSquare[,] moWorldSquares;
        // Player Square
        PlayerSquare moPlayerSquare;
        // A List of Enemies
        List<Enemy> miEnemies;
        // Pseudo-Random Number Generator
        Random moRandomNoGenerator;
        // A list of Particles
        List<Particle[]> miParticles;
        private int mdActiveParticles;

        // A Collection of all Textures
        TextureCollection moTextures;
        // A Collection of all Fonts
        SpriteFontCollection moSpriteFonts;
        // A Collection of All Sounds
        SoundCollection moSounds;

        // Game State Objects
        SplashScreen moSplashScreen;
        MainMenu moMainMenu;
        OptionsMenu moOptionsMenu;

        // Player's Name handling
        string mdPlayerNameText;
        Vector2 moPlayerNamePosition;
        KeyboardState
            moKeyboardState,
            moKeyboardOldState;
        MouseState
            moMouseState,
            moMouseOldState;
        // Pause Menu
        Vector2 moPausePosition;
        Button
            moBtnContinue,
            moBtnSave,
            moBtnExit;
        bool mdButtonPressed;
        // Game Over
        double mdCounter;
        Vector2
            moGameOveMessagePosition,
            moGameOverInstructionsPosition;
        // Save Game
        bool mdSaveGameLoaded;

        //
		///
		//// CONSTANT DECLARATION
		///
		//

        /* Initial Game Background colour */
        private readonly Color GameBackgroundColour = Color.Black;
        /* STARTING GAME STATE */
        private const GameState StartingGameState = GameState.SplashScreen;
        // Default no filter colour for spritebatch draw
        private readonly Color NoFilter = Color.White;
        // Origin Vector for drawing background textures
        private readonly Vector2 Origin = Vector2.Zero;
        // For drawing the Game over message
        private const string GameOverCongratz = "Thanks For Playing !";
        private const string GameOverInstructions = "Press Escape to return to Main Menu";
        private const string GameOverFail = "You have died !";
        // For drawing the Menu's Title and Placeholder
        private const int MenuTitleX = 120;
        private const int MenuTitleY = 100;
        private readonly Vector2 MenuTitlePosition = new Vector2(MenuTitleX, MenuTitleY);
        private readonly Color MenuTitleColour = Color.WhiteSmoke;
        private const string MenuTitleText = "Amazing Squares Arcade Game";
        private const int MenuTitleOffset = 40;
        private const int MenuTitlePlaceholderWidth = 700;
        private const int MenuTitlePlaceholderHeight = 60;
        private readonly Rectangle MenuTitlePlaceholder = new Rectangle(
            MenuTitleX - MenuTitleOffset,
            MenuTitleY,
            MenuTitlePlaceholderWidth,
            MenuTitlePlaceholderHeight);
        // Maximum size of the World (Width and Height)
        private const int MaxWorldWidth = 40;   // Collumns
        private const int MaxWorldHeight = 20;  // Rows
        private const int LastLevel = 4;
        public static int MaxLevel = LastLevel;
        // Initial Capacity for the Enemies list
        private const int InitialEnemyListCapacity = 10;
        // Initial Capacity for the Particles List
        private const int InitialParticleListCapacity = 100;
        // Max amount of particles per particle generation
        private const int MaxParticles = 100;
        // Particle Generation Constants
        private const float BulletWallImpactParticleAngle = 80f;
        private const int BulletWallImpactNoOfParticles = 50;
        private const float BulletWallImpactParticleSpeedMultiplier = 3f;
        private const float PlayerLandImpactParticleAngle = 20f;
        private const float PlayerLandImpactParticleSpeed = 1f;
        private const int PlayerLandImpactNoOfParticles = 6;
        private const float PlayerDeathParticleAngle = 180;
        private const float PlayerDeathParticleSpeed = 10f;
        private const int PlayerDeathNoOfParticles = 100;
        // Player Name Constants
        private const string ProfileNameText = "Player Name:";
        private const int PlayerNameX = 30;
        private const int PlayerNameYOffset = 24;
        // Pause Indicator Constant
        private const float PauseXOffset = -80;
        /* PAUSE BUTTON CONSTANTS */
        private const int BtnWidth = 200;
        private const int BtnHeight = 50;
        private const int BtnX = 300;
        // Continue Button
        private const int BtnContinueX = BtnX;
        private const int BtnContinueY = 100;
        private readonly Rectangle BtnContinueRect = new Rectangle(
            BtnContinueX, BtnContinueY, BtnWidth, BtnHeight
            );
        private const string BtnContinueText = "Continue";
        // Save Button
        private const int BtnSaveX = BtnX;
        private const int BtnSaveY = BtnContinueY + 100;
        private readonly Rectangle BtnSaveRect = new Rectangle(
            BtnSaveX, BtnSaveY, BtnWidth, BtnHeight
            );
        private const string BtnSaveText = "Save Game";
        // Exit Button
        private const int BtnExitX = BtnX;
        private const int BtnExitY = BtnSaveY + 100;
        private readonly Rectangle BtnExitRect = new Rectangle(
            BtnExitX, BtnExitY, BtnWidth, BtnHeight
            );
        private const string BtnExitText = "Return to Menu";
        // Save Game Constants
        private const string SaveGameExtension = ".save";
        // GameOver WaitTime
        private const int PlayerDeadTime = 2;

        //
        ///
        //// CONSTRUCTOR
        ///
        //

        /// <summary>
        /// Game Constructor,
        /// Will create our game object,
        /// used to pre-initialize any object/variables.
        /// </summary>
        public Game()
        {
            moGraphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
            this.IsMouseVisible = true;
        }

        //
        ///
		//// XNA MAIN METHODS
		///
		//

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            /*** INITIALIZE ALL VARIABLES HERE ***/

            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);

            // Setting the starting game state
            meGameState = StartingGameState;
            // Sets the initial run once to true
            mdRunOnce = false;
            mdPaused = false;
            // Setting initial Level
            mdLevel = 1;
            // Creating the array for the world squares
            moWorldSquares = new WorldSquare[MaxWorldHeight, MaxWorldWidth];
            for (int row = 0; row < MaxWorldHeight; row++)
            {
                for (int col = 0; col < MaxWorldWidth; col++)
                {
                    moWorldSquares[row, col] = new WorldSquare(row, col);
                }
            }
            // Creating the texture variables
            moTextures = new TextureCollection();
            // Creating the Font variables
            moSpriteFonts = new SpriteFontCollection();
            // Creating the Sound variables
            moSounds = new SoundCollection();
            // Creating the Pseudo-Random Number Generator
            moRandomNoGenerator = new Random();
            // Creating the player controlled square
            moPlayerSquare = new PlayerSquare();
            // Creating the List of Enemies
            miEnemies = new List<Enemy>(InitialEnemyListCapacity);
            // Setting the Initial List Size
            miParticles = new List<Particle[]>(InitialParticleListCapacity);
            for (int ldListIndex = 0; ldListIndex < miParticles.Capacity; ldListIndex++)
            {
                miParticles.Add(new Particle[MaxParticles]);
                for (int ldArrayIndex = 0; ldArrayIndex < miParticles[ldListIndex].Length; ldArrayIndex++)
                {
                    miParticles[ldListIndex][ldArrayIndex] = new Particle(moRandomNoGenerator);
                }
            }
            mdActiveParticles = 0;
            mdCounter = 0;

            moPlayerNamePosition = new Vector2(0f);
            mdPlayerNameText = "";

            moPausePosition = new Vector2(0f);
            mdSaveGameLoaded = false;

            // Sets the Game screen dimensions
            mdWindowWidth = moWorldSquares[0, 0].Size * MaxWorldWidth;
            mdWindowHeight = moWorldSquares[0, 0].Size * MaxWorldHeight;

            // Game State Object Initializations
            moSplashScreen = new SplashScreen();
            moMainMenu = new MainMenu();
            moOptionsMenu = new OptionsMenu();

            base.Initialize();
        }
        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            /*** LOAD ALL EXTERNAL CONTENT HERE ***/
            // Loading all external content into our Collection classes.
            moTextures.LoadContent(this.Content);
            moSpriteFonts.LoadContent(this.Content);
            moSounds.LoadContent(this.Content);

            moSplashScreen.LoadContent(this.Content);
            moOptionsMenu.LoadContent(moSpriteFonts.OptionsMenuText);
            // Re-initializes some of the Game States objects that depend on external content
            moMainMenu = new MainMenu(moSpriteFonts.MainMenuTextFont);
            moBtnContinue = new Button(
                BtnContinueRect, moSpriteFonts.GameTextFont, MenuTitleColour, BtnContinueText
                );
            moBtnSave = new Button(
                BtnSaveRect, moSpriteFonts.GameTextFont, MenuTitleColour, BtnSaveText
                );
            moBtnExit = new Button(
                BtnExitRect, moSpriteFonts.GameTextFont, MenuTitleColour, BtnExitText
                );
        }
        
        #region Unload Content is ignored
        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// all content.
        /// </summary>
        protected override void UnloadContent()
        { }
        #endregion

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            /*  DEBUGGING COMMANDS  */
            RefreshKeyboard();
            /************************/

            switch (meGameState)
            {
                case GameState.SplashScreen:
                    SetScreenDimensions(ref moGraphics, moSplashScreen.Width, moSplashScreen.Height);
                    moSplashScreen.Update(ref meGameState, ref mdRunOnce, ref gameTime);
                    moPlayerSquare.Name = moSplashScreen.PlayerName;
                    break;
                case GameState.MainMenu:
                    SetScreenDimensions(ref moGraphics, moMainMenu.Width, moMainMenu.Height);
                    moMainMenu.Update(ref meGameState, ref mdRunOnce, ref mdSaveGameLoaded, ref mdLevel, moPlayerSquare.Name);
                    mdPlayerNameText = ProfileNameText + "\n" + moPlayerSquare.Name;
                    moPlayerNamePosition.X = PlayerNameX;
                    moPlayerNamePosition.Y = moMainMenu.Height - (PlayerNameYOffset * 2);
                    break;
                case GameState.Options:
                    SetScreenDimensions(ref moGraphics, moOptionsMenu.Width, moOptionsMenu.Height);
                    moOptionsMenu.Update(ref meGameState);
                    mdPlayerNameText = ProfileNameText + "\n" + moPlayerSquare.Name;
                    moPlayerNamePosition.X = PlayerNameX;
                    moPlayerNamePosition.Y = moOptionsMenu.Height - (PlayerNameYOffset * 2);
                    break;
                case GameState.GameOver:
                    MediaPlayer.Volume *= 0.995f;
                    if (moKeyboardState.IsKeyDown(Keys.Escape) == true)
                    {
                        meGameState = GameState.MainMenu;
                        MediaPlayer.Stop();
                        MediaPlayer.Volume = 1f;
                    }
                    Vector2 loMeasurements;
                    if (moPlayerSquare.Alive == true)
                    {
                        loMeasurements = moSpriteFonts.GameTextFont.MeasureString(GameOverCongratz);
                    }
                    else
                    {
                        loMeasurements = moSpriteFonts.GameTextFont.MeasureString(GameOverFail);
                    }
                    moGameOveMessagePosition.X = (mdWindowWidth / 2f) - (loMeasurements.X / 2f);
                    moGameOveMessagePosition.Y = (mdWindowHeight / 2f) - (loMeasurements.Y / 2f);
                    moGameOverInstructionsPosition.Y = (mdWindowHeight / 2f) + (loMeasurements.Y / 2f) + moSpriteFonts.GameTextFont.LineSpacing;
                    loMeasurements = moSpriteFonts.GameTextFont.MeasureString(GameOverInstructions);
                    moGameOverInstructionsPosition.X = (mdWindowWidth / 2f) - (loMeasurements.X / 2f);
                    break;
                case GameState.Loading:
                    LoadGame();
                    meGameState = GameState.InGame;
                    break;
                case GameState.InGame:
                    #region Debug Commands
                    if (moKeyboardState.IsKeyDown(Keys.Enter) &&
                        moKeyboardState.IsKeyDown(Keys.LeftShift) &&
                        moKeyboardOldState.IsKeyUp(Keys.Enter))
                    {
                        if (mdLevel != 1)
                        {
                            mdLevel -= 2;
                        }
                        LevelComplete();
                    }
                    else if (moKeyboardState.IsKeyDown(Keys.Enter) &&
                        moKeyboardOldState.IsKeyUp(Keys.Enter))
                    {
                        LevelComplete();
                    }
                    if (moKeyboardState.IsKeyDown(Keys.Back) == true)
                    {
                        meGameState = GameState.MainMenu;
                        mdRunOnce = true;
                    }
                    #endregion
                    GamePlay();
                    #region Player Dead Timer
                    if (moPlayerSquare.State == PlayerSquare.PlayerState.Dead)
                    {
                        if (mdCounter > PlayerDeadTime)
                        {
                            mdCounter = 0;
                            meGameState = GameState.GameOver;
                        }
                        else
                        {
                            mdCounter += gameTime.ElapsedGameTime.TotalSeconds;
                        }
                    }
                    #endregion
                    mdPlayerNameText = ProfileNameText + " " + moPlayerSquare.Name;
                    moPlayerNamePosition.X = PlayerNameX;
                    moPlayerNamePosition.Y = mdWindowHeight - PlayerNameYOffset;
                    if (mdPaused == true)
                    {
                        PauseMenu();
                    }
                    break;
                case GameState.Exit:
                    this.Exit();
                    break;
                default:
                    break;
            }

            base.Update(gameTime);
        }
        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(GameBackgroundColour);

            spriteBatch.Begin();

            switch (meGameState)
            {
                case GameState.SplashScreen:
                    moSplashScreen.Draw(spriteBatch);
                    break;
                case GameState.Loading:
                case GameState.MainMenu:
                    Draw(moMainMenu);
                    spriteBatch.DrawString(moSpriteFonts.GameTextFont, mdPlayerNameText, moPlayerNamePosition, Color.AntiqueWhite);
                    break;
                case GameState.Options:
                    Draw(moOptionsMenu);
                    spriteBatch.DrawString(moSpriteFonts.GameTextFont, mdPlayerNameText, moPlayerNamePosition, Color.AntiqueWhite);
                    break;
                case GameState.GameOver:
                case GameState.InGame:
                    Draw(moWorldSquares);
                    Draw(moPlayerSquare);
                    Draw(miEnemies);
                    Draw(miParticles);
                    spriteBatch.DrawString(moSpriteFonts.GameTextFont, mdPlayerNameText, moPlayerNamePosition, Color.Black);
                    if (mdPaused)
                    {
                        spriteBatch.DrawString(
                            moSpriteFonts.GameTextFont, "Pause", moPausePosition, Color.Black
                            );
                        Draw(moBtnContinue);
                        Draw(moBtnSave);
                        Draw(moBtnExit);
                    }
                    if (meGameState == GameState.GameOver)
                    {
                        spriteBatch.Draw(moTextures.ButtonActive, new Rectangle((int)Origin.X, (int)Origin.Y, mdWindowWidth, mdWindowHeight), NoFilter);
                        if (moPlayerSquare.Alive == true)
                        {
                            spriteBatch.DrawString(moSpriteFonts.GameTextFont, GameOverCongratz, moGameOveMessagePosition, NoFilter);
                        }
                        else
                        {
                            spriteBatch.DrawString(moSpriteFonts.GameTextFont, GameOverFail, moGameOveMessagePosition, NoFilter);
                        }
                        spriteBatch.DrawString(moSpriteFonts.GameTextFont, GameOverInstructions, moGameOverInstructionsPosition, NoFilter);
                    }
                    break;
                default:
                    break;
            }

            spriteBatch.End();

            base.Draw(gameTime);
        }

        //
		///
		//// PRIVATE METHODS
		///
		//

        /// <summary>
        /// Will handle all update logic,
        /// for the gameplay
        /// </summary>
        private void GamePlay()
        {
            if (moPlayerSquare.Alive == true)
            {
                if (moKeyboardState.IsKeyDown(Keys.Escape) &&
                    moKeyboardOldState.IsKeyUp(Keys.Escape))
                {
                    Pause();
                }
            }
            RunOnce();
            Track(moPlayerSquare);
            moPlayerSquare.Update();
            for (int i = 0; i < miEnemies.Count; i++)
            {
                Track(miEnemies[i]);
                miEnemies[i].Update();
            }
            for (int ldIndex = 0; ldIndex < miParticles.Capacity; ldIndex++)
            {
                for (int ldArrayIndex = 0; ldArrayIndex < miParticles[ldIndex].Length; ldArrayIndex++)
                {
                    miParticles[ldIndex][ldArrayIndex].Update();
                }
            }
            ComputeCollisions();
        }
        /// <summary>
        /// Will handle update logic,
        /// for the pause menu.
        /// </summary>
        private void PauseMenu()
        {
            moBtnContinue.Update();
            moBtnSave.Update();
            moBtnExit.Update();
            if (mdButtonPressed == false)
            {
                RefreshMouse();
                ComputeCollisions(moBtnContinue);
                ComputeCollisions(moBtnSave);
                ComputeCollisions(moBtnExit);
            }
            else
            {
                MediaPlayer.Volume *= 0.9f;
                if (moBtnContinue.IsPressed() == true)
                {
                    Pause();
                }
                else if (moBtnSave.IsPressed() == true)
                {
                    SaveGame();
                    Pause();
                }
                else if (moBtnExit.IsPressed() == true)
                {
                    meGameState = GameState.MainMenu;
                    MediaPlayer.Stop();
                    MediaPlayer.Volume = 1f;
                    Pause();
                }
            }
        }
        /// <summary>
        /// Will check,
        /// using the two Passed integer's as coordinates (x and y, respectively),
        /// if they are inside the Passed Rectangle.
        /// </summary>
        /// <param name="ldXCoord">
        /// Passed integer to be used as a X coordinate.
        /// </param>
        /// <param name="ldYCoord">
        /// Passed integer to be used as a Y coordinate.
        /// </param>
        /// <param name="loBoundingRect">
        /// Passed Rectangle to be checked if other two int's are inside Passed Rectangle.
        /// </param>
        /// <returns>
        /// Returns true if the Passed coordinates are inside the Rectangle,
        /// else false.
        /// </returns>
        private bool CheckIfIn(int ldXCoord, int ldYCoord, Rectangle loBoundingRect)
        {
            bool ldInside = false;
            if (ldXCoord > loBoundingRect.X &&
                ldXCoord < loBoundingRect.X + loBoundingRect.Width &&
                ldYCoord > loBoundingRect.Y &&
                ldYCoord < loBoundingRect.Y + loBoundingRect.Height)
            {
                ldInside = true;
            }
            return ldInside;
        }
        /// <summary>
        /// Will set all of the GamePlay entities to their Paused State.
        /// </summary>
        private void Pause()
        {
            moPausePosition.X = mdWindowWidth + PauseXOffset;
            moPausePosition.Y = moPlayerNamePosition.Y;
            moPlayerSquare.Pause();
            for (int i = 0; i < miEnemies.Count; i++)
            {
                miEnemies[i].Pause();
            }
            for (int ldIndex = 0; ldIndex < miParticles.Count; ldIndex++)
            {
                for (int ldArrayIndex = 0; ldArrayIndex < miParticles[ldIndex].Length; ldArrayIndex++)
                {
                    miParticles[ldIndex][ldArrayIndex].Pause();
                }
            }
            mdPaused = !mdPaused;
            if (mdPaused == true)
            {
                MediaPlayer.Volume = 0.5f;
                mdButtonPressed = false;
            }
            else
            {
                MediaPlayer.Volume = 1f;
                mdButtonPressed = false;
            }
        }
        /// <summary>
        /// Will compute all collisions between all game objects
        /// </summary>
        private void ComputeCollisions()
        {
            ComputeCollisions(moPlayerSquare);
            for (int i = 0; i < miEnemies.Count; i++)
            {
                ComputeCollisions(miEnemies[i]);
            }
        }
        /// <summary>
        /// Overloaded Method,
        /// will compute collisions with passed object,
        /// namely the PlayerSquare.
        /// </summary>
        /// <param name="loPlayer">
        /// Passed Player class.
        /// </param>
        private void ComputeCollisions(PlayerSquare loPlayer)
        {
            if (loPlayer.Col <= 0)
	        {
                loPlayer.SetCol(1);
                loPlayer.SetPosition(
                    moWorldSquares[loPlayer.Row + 1, loPlayer.Col].Position.X,
                    loPlayer.Position.Y);
	        }
            else if (loPlayer.Col >= MaxWorldWidth)
            {
                loPlayer.SetCol(MaxWorldWidth - 1);
                loPlayer.SetPosition(
                    loPlayer.Position.X,
                    moWorldSquares[loPlayer.Row - 1, loPlayer.Col].Position.Y + moWorldSquares[loPlayer.Row, loPlayer.Col].Size - loPlayer.Height);
            }
            if (loPlayer.Row <= 0)
            {
                loPlayer.SetRow(1);
                loPlayer.SetPosition(
                    loPlayer.Position.X,
                    moWorldSquares[loPlayer.Row + 1, loPlayer.Col].Position.Y);
            }
            else if (loPlayer.Row >= MaxWorldHeight)
            {
                loPlayer.SetRow(MaxWorldHeight - 1);
                loPlayer.SetPosition(
                    moWorldSquares[loPlayer.Row, loPlayer.Col - 1].Position.X + moWorldSquares[loPlayer.Row, loPlayer.Col].Size - loPlayer.Width,
                    loPlayer.Position.Y);
            }
                switch (loPlayer.State)
                {
                    case PlayerSquare.PlayerState.Dead:
                        break;
                    case PlayerSquare.PlayerState.Paused:
                        break;
                    case PlayerSquare.PlayerState.AirUp:
                        if (CheckIfWall(moWorldSquares[loPlayer.Row - 1, loPlayer.Col]) == true)
                        {
                            loPlayer.HitRoof(moWorldSquares[loPlayer.Row - 1, loPlayer.Col]);
                        }
                        goto case PlayerSquare.PlayerState.Alive;
                    case PlayerSquare.PlayerState.AirDown:
                        if (CheckIfWall(moWorldSquares[loPlayer.Row + 1, loPlayer.Col]) == true)
                        {
                            loPlayer.Landed(moWorldSquares[loPlayer.Row, loPlayer.Col]);
                            GenerateParticles(
                                miParticles[mdActiveParticles], PlayerLandImpactNoOfParticles, loPlayer.BottomRight,
                                PlayerLandImpactParticleSpeed, -PlayerLandImpactParticleSpeed, PlayerLandImpactParticleAngle
                                );
                            GenerateParticles(
                                miParticles[mdActiveParticles], PlayerLandImpactNoOfParticles, loPlayer.BottomLeft,
                                -PlayerLandImpactParticleSpeed, -PlayerLandImpactParticleSpeed, PlayerLandImpactParticleAngle
                                );
                        }
                        goto case PlayerSquare.PlayerState.Alive;
                    case PlayerSquare.PlayerState.Ground:
                        if (CheckIfWall(moWorldSquares[loPlayer.Row + 1, loPlayer.Col]) != true)
                        {
                            loPlayer.NoGround();
                        }
                        goto case PlayerSquare.PlayerState.Alive;
                    case PlayerSquare.PlayerState.Alive:
                        if (CheckIfWall(moWorldSquares[loPlayer.Row, loPlayer.Col - 1]) == true)    // wall to the left of the player
                        {
                            if (CheckIfWall(moWorldSquares[loPlayer.Row + 1, loPlayer.Col - 1]) == true)
                            {
                                if (loPlayer.Direction == PlayerSquare.Movement.AirLeft)
                                {
                                    loPlayer.HitCornerWall(moWorldSquares[loPlayer.Row + 1, loPlayer.Col - 1]);
                                }
                                else
                                {
                                    loPlayer.HitLeftWall(moWorldSquares[loPlayer.Row, loPlayer.Col]);
                                }
                            }
                            else if (CheckIfWall(moWorldSquares[loPlayer.Row + 1, loPlayer.Col + 1]) == true)
                            {
                                if (loPlayer.Direction == PlayerSquare.Movement.AirRight)
                                {
                                    loPlayer.HitCornerWall(moWorldSquares[loPlayer.Row + 1, loPlayer.Col + 1]);
                                }
                                else
                                {
                                    loPlayer.HitLeftWall(moWorldSquares[loPlayer.Row, loPlayer.Col]);
                                }
                            }
                            else
                            {
                                loPlayer.HitLeftWall(moWorldSquares[loPlayer.Row, loPlayer.Col]);
                            }
                        }
                        if (CheckIfWall(moWorldSquares[loPlayer.Row, loPlayer.Col + 1]) == true)    // wall to the right of the player
                        {
                            loPlayer.HitRightWall(moWorldSquares[loPlayer.Row, loPlayer.Col]);
                        }
                        if (CheckIfWall(moWorldSquares[loPlayer.Row, loPlayer.Col]) == true)        // emergency player is inside a wall
                        {
                            loPlayer.EjectOutOfWall(moWorldSquares[loPlayer.PreviousRow, loPlayer.PreviousCol]);
                        }
                        if (moWorldSquares[loPlayer.Row, loPlayer.Col].Type == WorldSquare.SquareType.Exit)
                        {
                            LevelComplete();
                        }
                        break;
                    default:
                        break;
                }
        }
        /// <summary>
        /// Will check of what sub class the Passed Enemy is,
        /// and run the appropriate collisions based on sub type.
        /// </summary>
        /// <param name="loEnemy">
        /// Passed Enemy that will be checked.
        /// </param>
        private void ComputeCollisions(Enemy loEnemy)
        {
            if (loEnemy is EnemySquareBasic)
            {
                ComputeCollisions(loEnemy as EnemySquareBasic);
            }
            else if (loEnemy is EnemySquareSemiBasic)
            {
                ComputeCollisions(loEnemy as EnemySquareSemiBasic);
            }
            else if (loEnemy is EnemySquareTurret)
            {
                ComputeCollisions(loEnemy as EnemySquareTurret);
            }
        }
        /// <summary>
        /// Overloaded Method,
        /// will compute collisions with passed object,
        /// namely the sub class EnemySquareBasic.
        /// </summary>
        /// <param name="loEnemy">
        /// Passed EnemyBasic sub class of Enemy base class.
        /// </param>
        private void ComputeCollisions(EnemySquareBasic loEnemyBasic)
        {
            switch (loEnemyBasic.State)
            {
                case Enemy.EnemyState.Dead:
                    break;
                case Enemy.EnemyState.Air:
                case Enemy.EnemyState.Ground:
                case Enemy.EnemyState.SeePlayer:
                case Enemy.EnemyState.Patrol:
                case Enemy.EnemyState.Attack:
                case Enemy.EnemyState.Alive:
                default:
                    if (CheckIfWall(moWorldSquares[loEnemyBasic.Row, loEnemyBasic.Col]) == true)
                    {
                        loEnemyBasic.InsideWall(moWorldSquares[loEnemyBasic.Row, loEnemyBasic.Col]);
                    }
                    else if (CheckIfWall(moWorldSquares[loEnemyBasic.Row, loEnemyBasic.Col + 1]) == true)
                    {
                        loEnemyBasic.HitRightWall(moWorldSquares[loEnemyBasic.Row, loEnemyBasic.Col]);
                    }
                    else if (CheckIfWall(moWorldSquares[loEnemyBasic.Row, loEnemyBasic.Col - 1]) == true)
                    {
                        loEnemyBasic.HitLeftWall(moWorldSquares[loEnemyBasic.Row, loEnemyBasic.Col]);
                    }
                    ComputeCollisions(moPlayerSquare, loEnemyBasic);
                    break;
            }
        }
        /// <summary>
        /// Overloaded Method,
        /// will compute collisions with passed object,
        /// namely the sub class EnemySquareSemiBasic.
        /// </summary>
        /// <param name="loEnemySemiBasic">
        /// Passed EnemySemiBasic sub class of Enemy base class.
        /// </param>
        private void ComputeCollisions(EnemySquareSemiBasic loEnemySemiBasic)
        {
            switch (loEnemySemiBasic.State)
            {
                case Enemy.EnemyState.Dead:
                    break;
                case Enemy.EnemyState.Air:
                case Enemy.EnemyState.Ground:
                case Enemy.EnemyState.SeePlayer:
                case Enemy.EnemyState.Patrol:
                case Enemy.EnemyState.Attack:
                case Enemy.EnemyState.Alive:
                default:
                    if (CheckIfWall(moWorldSquares[loEnemySemiBasic.Row, loEnemySemiBasic.Col]) == true)
                    {
                        loEnemySemiBasic.InsideWall(moWorldSquares[loEnemySemiBasic.Row, loEnemySemiBasic.Col]);
                    }
                    else if (CheckIfWall(moWorldSquares[loEnemySemiBasic.Row - 1, loEnemySemiBasic.Col]) == true)
                    {
                        loEnemySemiBasic.HitUpWall(moWorldSquares[loEnemySemiBasic.Row, loEnemySemiBasic.Col]);
                    }
                    else if (CheckIfWall(moWorldSquares[loEnemySemiBasic.Row + 1, loEnemySemiBasic.Col]) == true)
                    {
                        loEnemySemiBasic.HitDownWall(moWorldSquares[loEnemySemiBasic.Row, loEnemySemiBasic.Col]);
                    }
                    ComputeCollisions(moPlayerSquare, loEnemySemiBasic);
                    break;
            }
        }
        /// <summary>
        /// Overloaded Method,
        /// will compute collisions with passed object,
        /// namely the sub class EnemySquareTurret.
        /// </summary>
        /// <param name="loEnemyTurret">
        /// Passed EnemyTurret sub class of Enemy base class.
        /// </param>
        private void ComputeCollisions(EnemySquareTurret loEnemyTurret)
        {
            switch (loEnemyTurret.State)
            {
                case Enemy.EnemyState.Dead:
                    break;
                case Enemy.EnemyState.SeePlayer:
                case Enemy.EnemyState.Patrol:
                case Enemy.EnemyState.Attack:
                case Enemy.EnemyState.Air:
                case Enemy.EnemyState.Ground:
                case Enemy.EnemyState.Alive:
                default:
                    for (int i = 0; i < loEnemyTurret.Bullets.Length; i++)
                    {
                        if (loEnemyTurret.Bullets[i].Alive)
                        {
                            ComputeCollisions(loEnemyTurret.Bullets[i]);
                            ComputeCollisions(moPlayerSquare, loEnemyTurret.Bullets[i]);
                        }
                    }
                    ComputeCollisions(moPlayerSquare, loEnemyTurret);
                    break;
            }
        }
        /// <summary>
        /// Overloaded Method,
        /// will compute collisions between the passed objects,
        /// namely the PlayerSquare class and the EnemySquareBasic class,
        /// if basic enemy collides into player then player dies.
        /// </summary>
        /// <param name="loPlayer">
        /// Passed PlayerSquare class.
        /// </param>
        /// <param name="loEnemySquareBasic">
        /// Passed EnemySquareBasic sub class of Enemy base class.
        /// </param>
        private void ComputeCollisions(PlayerSquare loPlayer, EnemySquareBasic loEnemySquareBasic)
        {
            if (
                (loPlayer.State != PlayerSquare.PlayerState.Dead && loPlayer.State != PlayerSquare.PlayerState.Paused)
                &&
                (loPlayer.Row <= loEnemySquareBasic.Row + 1 && loPlayer.Row >= loEnemySquareBasic.Row - 1)
                &&
                (loPlayer.Col <= loEnemySquareBasic.Col + 1 && loPlayer.Col >= loEnemySquareBasic.Col - 1)
                )
            {
                if (
                    (loPlayer.Position.X >= loEnemySquareBasic.Position.X - loPlayer.Width &&
                    loPlayer.Position.X <= loEnemySquareBasic.Position.X + loEnemySquareBasic.Width) &&
                    (loPlayer.Position.Y >= loEnemySquareBasic.Position.Y - loPlayer.Height &&
                    loPlayer.Position.Y <= loEnemySquareBasic.Position.Y + loEnemySquareBasic.Height)
                    )
                {
                    Kill(loPlayer);
                }
            }
        }
        /// <summary>
        /// Overloaded Method,
        /// will compute collisions between the passed objects,
        /// namely the PlayerSquare class and the EnemySquareSemiBasic class,
        /// if semi basic enemy collides into player then player dies.
        /// </summary>
        /// <param name="loPlayer">
        /// Passed PlayerSquare class.
        /// </param>
        /// <param name="loEnemySquareSemiBasic">
        /// Passed EnemySquareSemiBasic sub class of Enemy base class.
        /// </param>
        private void ComputeCollisions(PlayerSquare loPlayer, EnemySquareSemiBasic loEnemySquareSemiBasic)
        {
            if (
                (loPlayer.State != PlayerSquare.PlayerState.Dead && loPlayer.State != PlayerSquare.PlayerState.Paused)
                &&
                (loPlayer.Row <= loEnemySquareSemiBasic.Row + 1 && loPlayer.Row >= loEnemySquareSemiBasic.Row - 1)
                &&
                (loPlayer.Col <= loEnemySquareSemiBasic.Col + 1 && loPlayer.Col >= loEnemySquareSemiBasic.Col - 1)
                )
            {
                if (
                    (loPlayer.Position.X >= loEnemySquareSemiBasic.Position.X - loPlayer.Width &&
                    loPlayer.Position.X <= loEnemySquareSemiBasic.Position.X + loEnemySquareSemiBasic.Width) &&
                    (loPlayer.Position.Y >= loEnemySquareSemiBasic.Position.Y - loPlayer.Height &&
                    loPlayer.Position.Y <= loEnemySquareSemiBasic.Position.Y + loEnemySquareSemiBasic.Height)
                    )
                {
                    Kill(loPlayer);
                }
            }
        }
        /// <summary>
        /// Overloaded Method:
        /// will compute collisions between the passed objects,
        /// namely the PlayerSquare class and the EnemySquareTurret class,
        /// if player collides into enemy turret then player dies.
        /// </summary>
        /// <param name="loPlayer">
        /// Passed PlayerSquare class.
        /// </param>
        /// <param name="loEnemySquareTurret">
        /// Passed EnemySquareTurret sub class of Enemy base class.
        /// </param>
        private void ComputeCollisions(PlayerSquare loPlayer, EnemySquareTurret loEnemySquareTurret)
        {
            if (
                (loPlayer.State != PlayerSquare.PlayerState.Dead && loPlayer.State != PlayerSquare.PlayerState.Paused)
                &&
                (loPlayer.Row <= loEnemySquareTurret.Row + 1 && loPlayer.Row >= loEnemySquareTurret.Row - 1)
                &&
                (loPlayer.Col <= loEnemySquareTurret.Col + 1 && loPlayer.Col >= loEnemySquareTurret.Col - 1)
                )
            {
                if (
                    (loPlayer.Position.X >= loEnemySquareTurret.Position.X - loPlayer.Width &&
                    loPlayer.Position.X <= loEnemySquareTurret.Position.X + loEnemySquareTurret.Width) &&
                    (loPlayer.Position.Y >= loEnemySquareTurret.Position.Y - loPlayer.Height &&
                    loPlayer.Position.Y <= loEnemySquareTurret.Position.Y + loEnemySquareTurret.Height)
                    )
                {
                    Kill(loPlayer);
                }
            }
        }
        /// <summary>
        /// Overloaded Method:
        /// will compute collisions between the passed objects,
        /// namely the PlayerSquare class and the bullet class,
        /// if player collides into the bullet then player dies.
        /// </summary>
        /// <param name="loPlayer">
        /// Passed PlayerSquare class.
        /// </param>
        /// <param name="loBullet">
        /// Passed Bullet class.
        /// </param>
        private void ComputeCollisions(PlayerSquare loPlayer, Bullet loBullet)
        {
            if (
                (loPlayer.State != PlayerSquare.PlayerState.Dead && loPlayer.State != PlayerSquare.PlayerState.Paused)
                &&
                (loPlayer.Row <= loBullet.Row + 1 && loPlayer.Row >= loBullet.Row - 1)
                &&
                (loPlayer.Col <= loBullet.Col + 1 && loPlayer.Col >= loBullet.Col - 1)
                )
            {
                if (
                    (loPlayer.Position.X >= loBullet.Position.X - loPlayer.Width &&
                    loPlayer.Position.X <= loBullet.Position.X + loBullet.Width) &&
                    (loPlayer.Position.Y >= loBullet.Position.Y - loPlayer.Height &&
                    loPlayer.Position.Y <= loBullet.Position.Y + loBullet.Height)
                    )
                {
                    Kill(loPlayer);
                    loBullet.Die();
                }
            }
        }
        /// <summary>
        /// Overloaded Method:
        /// will compute collisions between the passed object,
        /// namely the Bullet class,
        /// if bullet collides into a wall it will die
        /// </summary>
        /// <param name="loBullet">
        /// Passed Bullet class.
        /// </param>
        private void ComputeCollisions(Bullet loBullet)
        {
            if (loBullet.RotationAngle == 0)
            {
                if ( CheckIfWall(moWorldSquares[loBullet.Row - 1, loBullet.Col]) == true)
                {
                    if (loBullet.Position.Y < moWorldSquares[loBullet.Row, loBullet.Col].Position.Y)
                    {
                        GenerateParticles(
                            miParticles[mdActiveParticles],
                            BulletWallImpactNoOfParticles,
                            loBullet.Position,
                            -loBullet.Velocity.X * BulletWallImpactParticleSpeedMultiplier,
                            -loBullet.Velocity.Y * BulletWallImpactParticleSpeedMultiplier,
                            BulletWallImpactParticleAngle
                        );
                    }
                    loBullet.HitWallUp(moWorldSquares[loBullet.Row, loBullet.Col]);
                }
            }
            else if (loBullet.RotationAngle == 90)
            {
                if (CheckIfWall(moWorldSquares[loBullet.Row, loBullet.Col + 1]) == true)
                {
                    if (
                        loBullet.Position.X >
                        moWorldSquares[loBullet.Row, loBullet.Col].Position.X + moWorldSquares[0,0].Size
                        )
                    {
                        GenerateParticles(
                            miParticles[mdActiveParticles],
                            BulletWallImpactNoOfParticles,
                            loBullet.Position,
                            -loBullet.Velocity.X * BulletWallImpactParticleSpeedMultiplier,
                            -loBullet.Velocity.Y * BulletWallImpactParticleSpeedMultiplier,
                            BulletWallImpactParticleAngle
                        );
                    }
                    loBullet.HitWallRight(moWorldSquares[loBullet.Row, loBullet.Col]);
                }
            }
            else if (loBullet.RotationAngle == 180)
            {
                if (CheckIfWall(moWorldSquares[loBullet.Row + 1, loBullet.Col]) == true)
                {
                    if (loBullet.Position.Y >
                        moWorldSquares[loBullet.Row, loBullet.Col].Position.Y + moWorldSquares[0,0].Size)
                    {
                        GenerateParticles(
                            miParticles[mdActiveParticles],
                            BulletWallImpactNoOfParticles,
                            loBullet.Position,
                            -loBullet.Velocity.X * BulletWallImpactParticleSpeedMultiplier,
                            -loBullet.Velocity.Y * BulletWallImpactParticleSpeedMultiplier,
                            BulletWallImpactParticleAngle
                        );
                    }
                    loBullet.HitWallDown(moWorldSquares[loBullet.Row, loBullet.Col]);
                }
            }
            else if (loBullet.RotationAngle == 270)
            {
                if (CheckIfWall(moWorldSquares[loBullet.Row, loBullet.Col - 1]) == true)
                {
                    if (loBullet.Position.X <
                        moWorldSquares[loBullet.Row, loBullet.Col].Position.X)
                    {
                        GenerateParticles(
                            miParticles[mdActiveParticles],
                            BulletWallImpactNoOfParticles,
                            loBullet.Position,
                            -loBullet.Velocity.X * BulletWallImpactParticleSpeedMultiplier,
                            -loBullet.Velocity.Y * BulletWallImpactParticleSpeedMultiplier,
                            BulletWallImpactParticleAngle
                        );
                    }
                    loBullet.HitWallLeft(moWorldSquares[loBullet.Row, loBullet.Col]);
                }
            }
            else
            {
                throw new MissingFieldException("the Rotation angle for (" +
                    loBullet.Position.X.ToString("n2") + ","
                    + loBullet.Position.Y.ToString("n2") + ")"
                    );
            }
        }
        /// <summary>
        /// Will Compute all mouse Collisions with the Passed Button
        /// and set the button to its appropriate state,
        /// only if no other button has been pressed.
        /// </summary>
        /// <param name="loButton">
        /// Passed Button that will be checked against the mouse position.
        /// </param>
        private void ComputeCollisions(Button loButton)
        {
            if (loButton.IsActive() == true)
            {
                if (loButton.IsBeingPressed() == false)
                {
                    if (CheckIfIn(moMouseState.X, moMouseState.Y, loButton.Bounds) == true)
                    {
                        if (moMouseState.LeftButton == ButtonState.Pressed &&
                            moMouseOldState.LeftButton == ButtonState.Released)
                        {
                            loButton.Pressed();
                            mdButtonPressed = true;
                        }
                        else if (loButton.State == Button.ButtonState.Active)
                        {
                            loButton.Hovered();
                        }
                    }
                    else
                    {
                        loButton.SetActive();
                    }
                }
            }
        }
        /// <summary>
        /// Will set the Passed PlayerSquare to dead,
        /// and run the appropriate effects.
        /// </summary>
        /// <param name="loPlayer">
        /// Passed PlayerSquare that will be set to his dead state.
        /// </param>
        private void Kill(PlayerSquare loPlayer)
        {
            loPlayer.Die();
            GenerateParticles(
                miParticles[mdActiveParticles], PlayerDeathNoOfParticles,
                loPlayer.Center, 0f, PlayerDeathParticleSpeed, PlayerDeathParticleAngle
                );
            mdCounter = 0;
        }
        /// <summary>
        /// Overloaded Method:
        /// Will actively track the passed PlayerSquare around the WorldSquare array,
        /// and update it's row and collumn values.
        /// </summary>
        private void Track(PlayerSquare loPlayerSquare)
        {
            WorldSquare loCurrentSquare = moWorldSquares[loPlayerSquare.Row, loPlayerSquare.Col];
            loPlayerSquare.PreviousRow = loPlayerSquare.Row;
            loPlayerSquare.PreviousCol = loPlayerSquare.Col;
            if (loPlayerSquare.Position.Y < loCurrentSquare.Position.Y)
            {
                loPlayerSquare.Row--;
            }
            else if (loPlayerSquare.Position.Y > loCurrentSquare.Position.Y + loCurrentSquare.Size)
            {
                loPlayerSquare.Row++;
            }
            if (loPlayerSquare.Position.X < loCurrentSquare.Position.X)
            {
                loPlayerSquare.Col--;
            }
            else if (loPlayerSquare.Position.X > loCurrentSquare.Position.X + loCurrentSquare.Size)
            {
                loPlayerSquare.Col++;
            }
        }
        /// <summary>
        /// Overloaded Method:
        /// Will check what sub class this Passed Enemy base class is,
        /// and run the appropriate Track() method for each sub class.
        /// </summary>
        /// <param name="loEnemy">
        /// Passed base class Enemy, expected to get sub types of this class.
        /// </param>
        private void Track(Enemy loEnemy)
        {
            if (loEnemy is EnemySquareBasic)
            {
                Track(loEnemy as EnemySquareBasic);
            }
            else if (loEnemy is EnemySquareSemiBasic)
            {
                Track(loEnemy as EnemySquareSemiBasic);
            }
            else if (loEnemy is EnemySquareTurret)
            {
                Track(loEnemy as EnemySquareTurret);
            }
        }
        /// <summary>
        /// Overloaded Method:
        /// Will actively track the passed EnemySquareBasic around the WorldSquare array,
        /// and update it's row and collumn values.
        /// </summary>
        /// <param name="loEnemyBasic">
        /// Passed EnemyBasic sub class.
        /// </param>
        private void Track(EnemySquareBasic loEnemyBasic)
        {
            WorldSquare loCurrentSquare = moWorldSquares[loEnemyBasic.Row, loEnemyBasic.Col];
            loEnemyBasic.PreviousCol = loEnemyBasic.Col;
            loEnemyBasic.PreviousRow = loEnemyBasic.Row;
            if (loEnemyBasic.Position.Y < loCurrentSquare.Position.Y)
            {
                loEnemyBasic.Row--;
            }
            else if (loEnemyBasic.Position.Y > loCurrentSquare.Position.Y + loCurrentSquare.Size)
            {
                loEnemyBasic.Row++;
            }
            if (loEnemyBasic.Position.X < loCurrentSquare.Position.X)
            {
                loEnemyBasic.Col--;
            }
            else if (loEnemyBasic.Position.X > loCurrentSquare.Position.X + loCurrentSquare.Size)
            {
                loEnemyBasic.Col++;
            }
        }
        /// <summary>
        /// Overloaded Method:
        /// Will actively track the passed EnemySquareSemiBasic around the WorldSquare array,
        /// and update it's row and collumn values.
        /// </summary>
        /// <param name="loEnemySemiBasic">
        /// Passed EnemySemiBasic sub class.
        /// </param>
        private void Track(EnemySquareSemiBasic loEnemySemiBasic)
        {
            WorldSquare loCurrentSquare = moWorldSquares[loEnemySemiBasic.Row, loEnemySemiBasic.Col];
            loEnemySemiBasic.PreviousCol = loEnemySemiBasic.Col;
            loEnemySemiBasic.PreviousRow = loEnemySemiBasic.Row;
            if (loEnemySemiBasic.Position.Y < loCurrentSquare.Position.Y)
            {
                loEnemySemiBasic.Row--;
            }
            else if (loEnemySemiBasic.Position.Y > loCurrentSquare.Position.Y + loCurrentSquare.Size)
            {
                loEnemySemiBasic.Row++;
            }
            if (loEnemySemiBasic.Position.X < loCurrentSquare.Position.X)
            {
                loEnemySemiBasic.Col--;
            }
            else if (loEnemySemiBasic.Position.X > loCurrentSquare.Position.X + loCurrentSquare.Size)
            {
                loEnemySemiBasic.Col++;
            }
        }
        /// <summary>
        /// Overloaded Method:
        /// Will actively track the turrets alive bullets.
        /// </summary>
        /// <param name="loEnemyTurret">
        /// Passed EnemyTurret sub class.
        /// </param>
        private void Track(EnemySquareTurret loEnemyTurret)
        {
            for (int ldIndex = 0; ldIndex < loEnemyTurret.Bullets.Length; ldIndex++)
            {
                if (loEnemyTurret.Bullets[ldIndex].Alive)
                {
                    WorldSquare loCurrentSquare = moWorldSquares[loEnemyTurret.Bullets[ldIndex].Row, loEnemyTurret.Bullets[ldIndex].Col];
                    if (loEnemyTurret.Bullets[ldIndex].Position.Y < loCurrentSquare.Position.Y)
                    {
                        loEnemyTurret.Bullets[ldIndex].Row--;
                    }
                    else if (loEnemyTurret.Bullets[ldIndex].Position.Y > loCurrentSquare.Position.Y + loCurrentSquare.Size)
                    {
                        loEnemyTurret.Bullets[ldIndex].Row++;
                    }
                    if (loEnemyTurret.Bullets[ldIndex].Position.X < loCurrentSquare.Position.X)
                    {
                        loEnemyTurret.Bullets[ldIndex].Col--;
                    }
                    else if (loEnemyTurret.Bullets[ldIndex].Position.X > loCurrentSquare.Position.X + loCurrentSquare.Size)
                    {
                        loEnemyTurret.Bullets[ldIndex].Col++;
                    }
                }
            }
        }
        /// <summary>
        /// Checks if the passed WorldSquare is a Wall.
        /// </summary>
        /// <param name="loSquareToCheck">
        /// Passed WorldSquare to be checked
        /// </param>
        /// <returns>
        /// Returns True if the WorldSquare is a Wall,
        /// else it returns false.
        /// </returns>
        private bool CheckIfWall(WorldSquare loSquareToCheck)
        {
            bool ldItsAWall = false;
            switch (loSquareToCheck.Type)
            {
                case WorldSquare.SquareType.Wall:
                    ldItsAWall = true;
                    break;
                case WorldSquare.SquareType.Exit:
                case WorldSquare.SquareType.Empty:
                default:
                    break;
            }
            return ldItsAWall;
        }
        /// <summary>
        /// Itterates through WorldSquares,
        /// draws them based on their type.
        /// This is done so our WorldTextures is not declared in multiple locations
        /// </summary>
        private void Draw(WorldSquare[,] laWorldSquare)
        {
            for (int row = 0; row < MaxWorldHeight; row++)
            {
                for (int col = 0; col < MaxWorldWidth; col++)
                {
                    switch (moWorldSquares[row, col].Type)
                    {
                        case WorldSquare.SquareType.Wall:
                            spriteBatch.Draw(moTextures.WallSquare, laWorldSquare[row, col].Position, NoFilter);
                            break;
                        case WorldSquare.SquareType.Exit:
                            spriteBatch.Draw(moTextures.ExitSquare, laWorldSquare[row, col].Position, NoFilter);
                            break;
                        case WorldSquare.SquareType.Empty:
                        default:
                            spriteBatch.Draw(moTextures.EmptySquare, laWorldSquare[row, col].Position, NoFilter);
                            break;
                    }
                }
            }
        }
        /// <summary>
        /// Will draw the player
        /// </summary>
        private void Draw(PlayerSquare loPlayer)
        {
            if (loPlayer.State != PlayerSquare.PlayerState.Paused)
            {
                switch (loPlayer.State)
                {
                    case PlayerSquare.PlayerState.Dead:
                        break;
                    case PlayerSquare.PlayerState.AirUp:
                    case PlayerSquare.PlayerState.Ground:
                    case PlayerSquare.PlayerState.Alive:
                    default:
                        spriteBatch.Draw(moTextures.PlayerSquare, loPlayer.Position, NoFilter);
                        spriteBatch.Draw(moTextures.PlayerEyeSquare, loPlayer.EyePosition, NoFilter);
                        break;
                }
            }
            else
            {
                switch (loPlayer.StateOld)
                {
                    case PlayerSquare.PlayerState.Dead:
                        break;
                    case PlayerSquare.PlayerState.AirUp:
                    case PlayerSquare.PlayerState.Ground:
                    case PlayerSquare.PlayerState.Alive:
                    default:
                        spriteBatch.Draw(moTextures.PlayerSquare, loPlayer.Position, NoFilter);
                        spriteBatch.Draw(moTextures.PlayerEyeSquare, loPlayer.EyePosition, NoFilter);
                        break;
                }
            }
        }
        /// <summary>
        /// Will draw our list of enemies based on their sub-class
        /// </summary>
        private void Draw(List<Enemy> liEnemies)
        {
            for (int i = 0; i < liEnemies.Count; i++)
            {
                switch (liEnemies[i].State)
                {
                    case Enemy.EnemyState.Dead:
                        break;
                    case Enemy.EnemyState.Pause:
                    case Enemy.EnemyState.SeePlayer:
                    case Enemy.EnemyState.Patrol:
                    case Enemy.EnemyState.Attack:
                    case Enemy.EnemyState.Air:
                    case Enemy.EnemyState.Ground:
                    case Enemy.EnemyState.Alive:
                    default:
                        if (liEnemies[i] is EnemySquareBasic)
                        {
                            spriteBatch.Draw(moTextures.EnemyBasicSquare, miEnemies[i].Position, NoFilter);
                        }
                        else if (liEnemies[i] is EnemySquareSemiBasic)
                        {
                            spriteBatch.Draw(moTextures.EnemyBasicSquare, liEnemies[i].Position, NoFilter);
                        }
                        else if (liEnemies[i] is EnemySquareTurret)
                        {
                            Draw(spriteBatch, miEnemies[i] as EnemySquareTurret);
                        }
                        break;
                } // End of switch()
            }
        }
        /// <summary>
        /// Will draw the MainMenu elements
        /// </summary>
        private void Draw(MainMenu loMainMenu)
        {
            if (MediaPlayer.State != MediaState.Playing)
            {
                moSounds.Play(moSounds.MainMenuSong);
            }
            spriteBatch.Draw(moTextures.MainMenuBackground, Origin, NoFilter);
            spriteBatch.Draw(moTextures.Placeholder, MenuTitlePlaceholder, NoFilter);
            spriteBatch.DrawString(moSpriteFonts.TitleFont, MenuTitleText, MenuTitlePosition, MenuTitleColour);
            Draw(loMainMenu.ButtonStartGame);
            Draw(loMainMenu.ButtonContinueGame);
            Draw(loMainMenu.ButtonOptions);
            Draw(loMainMenu.ButtonExitGame);
            Draw(loMainMenu.ButtonHelpGame);
        }
        /// <summary>
        /// Will draw the OptionsMenu elements
        /// </summary>
        private void Draw(OptionsMenu loOptionsMenu)
        {
            spriteBatch.Draw(moTextures.MainMenuBackground, Origin, NoFilter);
            spriteBatch.Draw(moTextures.Placeholder, MenuTitlePlaceholder, NoFilter);
            spriteBatch.DrawString(moSpriteFonts.TitleFont, MenuTitleText, MenuTitlePosition, MenuTitleColour);
            Draw(loOptionsMenu.BtnBack);
            Draw(loOptionsMenu.BtnMute);
            Draw(loOptionsMenu.BtnRename);
        }
        /// <summary>
        /// Will draw the passed Button based on its state.
        /// </summary>
        /// <param name="loButton">
        /// Passed Button class to be drawn.
        /// </param>
        private void Draw(Button loButton)
        {
            switch (loButton.State)
            {
                case Button.ButtonState.Press:
                    spriteBatch.Draw(moTextures.ButtonPress, loButton.Bounds, NoFilter);
                    goto default;
                case Button.ButtonState.Hover:
                    spriteBatch.Draw(moTextures.ButtonHover, loButton.Bounds, NoFilter);
                    goto default;
                case Button.ButtonState.Deactive:
                    spriteBatch.Draw(moTextures.ButtonDeactive, loButton.Bounds, NoFilter);
                    break;
                case Button.ButtonState.Active:
                    spriteBatch.Draw(moTextures.ButtonActive, loButton.Bounds, NoFilter);
                    goto default;
                case Button.ButtonState.Pressed:
                default:
                    if (loButton.Text != null || loButton.Text != "")
                    {
                        spriteBatch.DrawString(loButton.Font, loButton.Text, loButton.TextPosition, loButton.FontColour);
                    }
                    break;
            }
        }
        /// <summary>
        /// Will draw the enemy turret,
        /// and all of its currently alive bullets.
        /// </summary>
        /// <param name="loSpriteBatch"></param>
        /// <param name="loEnemyTurret">
        /// Passed EnemyTurret class.
        /// </param>
        private void Draw(SpriteBatch loSpriteBatch, EnemySquareTurret loEnemyTurret)
        {
            loSpriteBatch.Draw(moTextures.EnemyTurretSquare, loEnemyTurret.Position, NoFilter);
            loSpriteBatch.Draw(moTextures.EnemyTurretEyeSquare, loEnemyTurret.EyePosition, NoFilter);
            for (int ldBulletIndex = 0; ldBulletIndex < loEnemyTurret.Bullets.Length; ldBulletIndex++)
            {
                if (loEnemyTurret.Bullets[ldBulletIndex].Alive)
                {
                    loSpriteBatch.Draw(
                        moTextures.EnemyTurretBullet,
                        loEnemyTurret.Bullets[ldBulletIndex].BoundaryBox, null, NoFilter,
                        loEnemyTurret.Bullets[ldBulletIndex].RotationAngleRad,
                        loEnemyTurret.Bullets[ldBulletIndex].CenterPosition,
                        SpriteEffects.None, 0f);
                }
            }
        }
        /// <summary>
        /// Will call other draw method by itterating through the Passed List.
        /// </summary>
        /// <param name="liParticles">
        /// Passed List of Particle array's, that will itterated through
        /// </param>
        private void Draw(List<Particle[]> liParticles)
        {
            for (int ldIndex = 0; ldIndex < liParticles.Capacity; ldIndex++)
            {
                Draw(liParticles[ldIndex]);
            }
        }
        /// <summary>
        /// Will draw all the alive particles,
        /// in the Passed Particle array.
        /// </summary>
        /// <param name="liParticles">
        /// Passed Particle[] that will be drawn, using it's Position Property.
        /// </param>
        private void Draw(Particle[] laParticles)
        {
            for (int i = 0; i < laParticles.Length; i++)
            {
                if (laParticles[i].Alive)
                {
                    spriteBatch.Draw(moTextures.Particle, laParticles[i].Position, NoFilter);
                }
            }
        }
        /// <summary>
        /// Will Allow the level to be reloaded,
        /// and will increment level.
        /// </summary>
        private void LevelComplete()
        {
            if (mdLevel >= LastLevel)
            {
                meGameState = GameState.GameOver;
            }
            else
            {
                mdLevel++;
                mdSaveGameLoaded = false;
                mdRunOnce = true;
            }
        }
        /// <summary>
        /// Will load in a level from our Levels folder
        /// </summary>
        /// <param name="ldLevel">
        /// Passed Integer that will define what level to load (0 = user edited level).
        /// </param>
        private void LoadLevel(int ldLevel)
        {
            // Detecting what level to load
            string ldLevelToLoad = FilePaths.Level;
            int ldEnemyCount = 0;
            switch (ldLevel)
            {
                case 4:
                    ldLevelToLoad += "4.csv";
                    goto default;
                case 3:
                    ldLevelToLoad += "3.csv";
                    goto default;
                case 2:
                    ldLevelToLoad += "2.csv";
                    goto default;
                case 1:
                    ldLevelToLoad += "1.csv";
                    goto default;
                default:
                    string[] laLevelLoaded,
                        laLevelContent = File.ReadAllLines(ldLevelToLoad);

                    // Deleting all the content of our Lists.
                    miEnemies.Clear();
                    miParticles.Clear();
                    // Creating new list.
                    miParticles = new List<Particle[]>(InitialParticleListCapacity);
                    for (int ldListIndex = 0; ldListIndex < miParticles.Capacity; ldListIndex++)
                    {
                        miParticles.Add(new Particle[MaxParticles]);
                        for (int ldArrayIndex = 0; ldArrayIndex < miParticles[ldListIndex].Length; ldArrayIndex++)
                        {
                            miParticles[ldListIndex][ldArrayIndex] = new Particle(moRandomNoGenerator);
                        }
                    }
                    mdActiveParticles = 0;
                    // Reading through the Level file.
                    for (int row = 0; row < MaxWorldHeight && row < laLevelContent.Length; row++)
                    {
                        laLevelLoaded = laLevelContent[row].Split(',');
                        for (int col = 0; col < MaxWorldWidth & col < laLevelLoaded.Length; col++)
                        {
                            switch (laLevelLoaded[col])
                            {
                                case "6":
                                    miEnemies.Insert(ldEnemyCount, new EnemySquareTurret());
                                    miEnemies[ldEnemyCount].SetPosition(col * moWorldSquares[0, 0].Size, row * moWorldSquares[0, 0].Size);
                                    miEnemies[ldEnemyCount].SetRowCollumn(row, col);
                                    ldEnemyCount++;
                                    goto default;
                                case "5":
                                    miEnemies.Insert(ldEnemyCount, new EnemySquareSemiBasic());
                                    miEnemies[ldEnemyCount].SetPosition(col * moWorldSquares[0, 0].Size, row * moWorldSquares[0, 0].Size);
                                    miEnemies[ldEnemyCount].SetRowCollumn(row, col);
                                    ldEnemyCount++;
                                    goto default;
                                case "4":
                                    miEnemies.Insert(ldEnemyCount, new EnemySquareBasic());
                                    miEnemies[ldEnemyCount].SetPosition(col * moWorldSquares[0, 0].Size, row * moWorldSquares[0, 0].Size);
                                    miEnemies[ldEnemyCount].SetRowCollumn(row, col);
                                    ldEnemyCount++;
                                    goto default;
                                case "3":
                                    moPlayerSquare.Spawn(col * moWorldSquares[0, 0].Size, row * moWorldSquares[0, 0].Size);
                                    moPlayerSquare.SetCol(col);
                                    moPlayerSquare.SetRow(row);
                                    moPlayerSquare.SetAlive();
                                    goto default;
                                case "2":
                                    moWorldSquares[row, col] = new WorldSquare(row, col, WorldSquare.SquareType.Exit);
                                    break;
                                case "1":
                                    moWorldSquares[row, col] = new WorldSquare(row, col, WorldSquare.SquareType.Wall);
                                    break;
                                case "0":
                                default:
                                    moWorldSquares[row, col] = new WorldSquare(row, col, WorldSquare.SquareType.Empty);
                                    break;
                            } // End of switch() 
                        }
                    }
                    break;
            } // End of switch()
        }
        /// <summary>
        /// Will save the games progression.
        /// </summary>
        private void SaveGame()
        {
            string[] ldSaveContents = new string[]
            {
                "Name:" + moPlayerSquare.Name,
                "Level:" + mdLevel.ToString(),
                "Row:" + moPlayerSquare.Row.ToString(),
                "Column:" + moPlayerSquare.Col.ToString()
            };
            string ldFileName = FilePaths.SaveGameFolder + moPlayerSquare.Name + SaveGameExtension;
            if (File.Exists(ldFileName) == true)
            {
                File.WriteAllLines(ldFileName, ldSaveContents);
            }
            else // It doesn't exist
            {
                File.Create(ldFileName);
                File.Open(ldFileName, FileMode.OpenOrCreate);
                File.WriteAllLines(ldFileName, ldSaveContents);
            }

        }
        /// <summary>
        /// Will load the save game.
        /// </summary>
        private void LoadGame()
        {
            string ldFileName = FilePaths.SaveGameFolder + moPlayerSquare.Name + SaveGameExtension;
            if (File.Exists(ldFileName) == true)
            {
                int[] laSaveGameData = new int[3];
                string[] laSaveLoaded = new string[4],
                    laSaveContents,
                    laSaveGame = File.ReadAllLines(ldFileName);
                for (int i = 0; i < laSaveGame.Length; i++)
                {
                    laSaveContents = laSaveGame[i].Split(':');
                    laSaveLoaded[i] = laSaveContents[1];
                    if (i != 0)
                    {
                        laSaveGameData[i - 1] = Convert.ToInt32(laSaveContents[1]);
                    }
                }
                moPlayerSquare.Name = laSaveLoaded[0];
                mdLevel = laSaveGameData[0];
                LoadLevel(mdLevel);
                moPlayerSquare.SetRow(laSaveGameData[1]);
                moPlayerSquare.SetCol(laSaveGameData[2]);
                moPlayerSquare.SetPosition(moPlayerSquare.Col * moWorldSquares[0, 0].Size, moPlayerSquare.Row * moWorldSquares[0, 0].Size);
                moPlayerSquare.SetAlive();
                mdSaveGameLoaded = true;
            }
        }
        /// <summary>
        /// Will set the Screen to the passed values
        /// </summary>
        /// <param name="loGraphics">
        /// Passed reference to Window Graphics manager variable.
        /// </param>
        /// <param name="ldWidth">
        /// Passed integer, to be set as the screen ConsWidth.
        /// </param>
        /// <param name="ldHeight">
        /// Passed integer, to be set as the screen ConHeight.
        /// </param>
        private void SetScreenDimensions(ref GraphicsDeviceManager loGraphics, int ldWidth, int ldHeight)
        {
            if (loGraphics.PreferredBackBufferWidth != ldWidth ||
                loGraphics.PreferredBackBufferHeight != ldHeight)
            {
                loGraphics.PreferredBackBufferWidth = ldWidth;
                loGraphics.PreferredBackBufferHeight = ldHeight;
                loGraphics.ApplyChanges();
            }
        }
        /// <summary>
        /// This code will run once in our game loop
        /// </summary>
        private void RunOnce()
        {
            if (mdRunOnce == false)
            {
                return;
            }
            else
            {
                SetScreenDimensions(ref moGraphics, mdWindowWidth, mdWindowHeight);
                if (mdSaveGameLoaded == false)
                {
                    LoadLevel(mdLevel);
                }
                else
                {
                    mdSaveGameLoaded = false;
                }
                MediaPlayer.Volume = 1f;
                moSounds.PlayLevelSong(mdLevel);
                mdCounter = 0;
                mdRunOnce = false;
            }
        }
        /// <summary>
        /// Will iterate through the Array Particles.
        /// </summary>
        /// <param name="laParticles">
        /// Passed Particle array.
        /// </param>
        /// <param name="loPosition">
        /// Passed Vector2 that will define the position of the Particle.
        /// </param>
        /// <param name="loDirection">
        /// Passed Vector2 that will define the velocity of the Particle.
        /// </param>
        /// <param name="ldAngleOfRotation">
        /// Passed float that will define the angle of rotation.
        /// </param>
        private void GenerateParticles(Particle[] laParticles, int ldNumberOfParticles,
            Vector2 loPosition, float ldVelocityX, float ldVelocityY, float ldAngleOfRotation)
        {
            if (ldNumberOfParticles > MaxParticles)
            {
                ldNumberOfParticles = MaxParticles;
            }
            for (int i = 0; i < ldNumberOfParticles; i++)
            {
                GenerateParticle(laParticles[i], loPosition, ldVelocityX, ldVelocityY, ldAngleOfRotation);
            }
            if (mdActiveParticles + 1 == InitialParticleListCapacity)
            {
                mdActiveParticles = 0;
            }
            else
            {
                mdActiveParticles++;
            }
        }
        /// <summary>
        /// Will generate the Passed Particle.
        /// </summary>
        /// <param name="laParticles">
        /// Passed Particle, that will be generated.
        /// </param>
        /// <param name="loPosition">
        /// Passed Vector2 that will define the position of the Particle.
        /// </param>
        /// <param name="loDirection">
        /// Passed Vector2 that will define the velocity of the Particle.
        /// </param>
        /// <param name="ldAngleOfRotation">
        /// Passed float that will define the angle of rotation.
        /// </param>
        private void GenerateParticle(Particle loParticle, Vector2 loPosition,
            float ldVelocityX, float ldVelocityY, float ldAngleOfRotation)
        {
            float ldPercentOfRotation = (float)(2f * moRandomNoGenerator.NextDouble()) - 1f;
            float ldPercentOfSpeed = (float)(1f * moRandomNoGenerator.NextDouble());
            Vector2 loDirection = new Vector2(ldVelocityX * ldPercentOfSpeed, ldVelocityY * ldPercentOfSpeed);
            loParticle.Activate(loPosition, loDirection, ldAngleOfRotation * ldPercentOfRotation);
        }
        /// <summary>
        /// Will update the keyboards states,
        /// to the current frame's presses,
        /// and the previous one's.
        /// </summary>
        private void RefreshKeyboard()
        {
            moKeyboardOldState = moKeyboardState;
            moKeyboardState = Keyboard.GetState();
        }
        /// <summary>
        /// Will update the mouses states,
        /// to the current frame's location/pressed,
        /// and the previous one's
        /// </summary>
        private void RefreshMouse()
        {
            moMouseOldState = moMouseState;
            moMouseState = Mouse.GetState();
        }

    }
} // End of namespace